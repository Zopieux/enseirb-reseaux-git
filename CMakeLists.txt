cmake_minimum_required(VERSION 2.6)

project(pirtoupire)

# For glib2
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMakeModules/")

# Tracker
add_subdirectory(src/tracker)

# Clients (Java with ant)
add_custom_target(${CMAKE_PROJECT_NAME}_clients
    ALL
    ant -q -noinput -f ${CMAKE_SOURCE_DIR}/src/client/build.xml build copydefaultconf makejars
)

# Centralized client (with tracker)
# add_subdirectory(src/client/centralized)

# Distributed client (standalone, without tracker)
# add_subdirectory(src/client/distributed)