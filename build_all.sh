#!/bin/sh

BUILDIR=build

rm -rf "$BUILDIR" 2>/dev/null
mkdir -p "$BUILDIR"
cd "$BUILDIR"
cmake ..
make all
