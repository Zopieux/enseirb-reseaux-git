#!/usr/bin/env python3
# coding: utf-8

import pyparsing as pyp

_MAX_LENGTH = 256

def criterion_matcher(criterion):
    """
    class TestObj(object):
        name = "tits.avi"
        key = "ABCDE1234"
        size = 64

    t = TestObj()

    matcher = criterion_matcher("size > 60 and key =~ abcd and (name = lol or name = tits.avi)")
    matcher(t)  # True
    """

    def QueryOpAnd(s, l, t):
        return lambda x: all(_(x) for _ in t[0][::2])

    def QueryOpOr(s, l, t):
        return lambda x: any(_(x) for _ in t[0][::2])

    def QueryOpNot(s, l, t):
        op, b = t[0]
        return lambda x: not b(x)

    def QueryEvalStr(s, l, t):
        op_map = {
            '=': lambda a, b: a == b,
            '==': lambda a, b: a == b,
            '~=': lambda a, b: a in b,
            '=~': lambda a, b: b in a,
        }
        a, op, b = t[0]
        return lambda x: op_map[op](a(x).lower(), b.lower())

    def QueryEvalInt(s, l, t):
        op_map = {
            '=': lambda a, b: a == b,
            '==': lambda a, b: a == b,
            '<': lambda a, b: a < b,
            '>': lambda a, b: a > b,
            '<=': lambda a, b: a <= b,
            '>=': lambda a, b: a >= b,
        }
        a, op, b = t[0]
        return lambda x: op_map[op](a(x), b)

    p_str_op = pyp.oneOf("= == ~= =~")
    p_int_op = pyp.oneOf("= == < > <= >=")

    p_str = pyp.Word(pyp.alphanums + "_.-", max=_MAX_LENGTH)
    p_int = pyp.Word(pyp.nums, max=_MAX_LENGTH).setParseAction(lambda s, l, t: [int(t[0])])

    p_fname = pyp.CaselessKeyword("name").setParseAction(lambda s, l, t: lambda obj: obj.name)
    p_key = pyp.CaselessKeyword("key").setParseAction(lambda s, l, t: lambda obj: obj.key)
    p_fsize = pyp.CaselessKeyword("size").setParseAction(lambda s, l, t: lambda obj: obj.size)

    p_crit = (
        pyp.Group((p_fname | p_key) + p_str_op + p_str).setParseAction(QueryEvalStr) |
        pyp.Group((p_fsize) + p_int_op + p_int).setParseAction(QueryEvalInt)
    )

    p_expr = pyp.operatorPrecedence(p_crit,[
        ("not", 1, pyp.opAssoc.RIGHT, QueryOpNot),
        ("and", 2, pyp.opAssoc.LEFT, QueryOpAnd),
        ("or", 2, pyp.opAssoc.LEFT, QueryOpOr),
    ])

    matcher = p_expr.parseString(criterion, parseAll=True)[0]
    matcher.__doc__ = "Match files for pattern `%s`" % criterion
    return matcher
