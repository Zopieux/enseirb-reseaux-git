#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals, print_function
import cmd
import hashlib
import logging
import multiprocessing as mp
import os
import shlex
import socket
import sys
import time
import traceback

PROMPT_CONNECTED = "[%s:%d] $ "
PROMPT_DISCONNECTED = "[not connected] $ "

if sys.stdout.isatty():
    def hilite(string, success=None, bold=False):
        attr = []
        if success is True:  # green
            attr.append('32')
        elif success is False:  # red
            attr.append('31')
        if bold:
            attr.append('1')
        return '\x1b[%sm%s\x1b[0m' % (';'.join(attr), string)

else:
    def hilite(*args, **kwargs):
        return string

def _debug(msg):
    sys.stderr.write(msg + "\n")
    sys.stderr.flush()

def _filehash(fname):
    hashed = hashlib.md5()
    with open(fname, 'rb') as f:
        hashed.update(f.read(2048))
    return hashed.hexdigest()


mp_manager = mp.Manager()
mp_data = mp_manager.Namespace()
mp_queue_send = mp_manager.Queue()
mp_queue_rec = mp_manager.Queue()
mp_data.seed = []
mp_data.leech = []
mp_data.alive = False
mp_data.announced = False
mp_lock = mp_manager.Lock()
mp_wait = mp_manager.Event()


def sync_thread(parent, sendq, data):
    time.sleep(5)
    while data.alive:
        if not data.announced:
            continue
        sendq.put((False, "update seed [] leech []"), block=True, timeout=1.0)
        time.sleep(5)

    _debug("EXITED FROM SYNC TIMED THREAD")


def socket_thread(parent, host, port, sendq, recq, data):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((host, port))
    except socket.error as e:
        data.alive = False
        data.announced = False
        # parent._notify_disconnected()
        recq.put((False, str(e)))
        mp_wait.set()
        return

    # success!
    data.alive = True
    # parent._notify_connected(host, port)
    mp_wait.set()

    sync_proc = mp.Process(target=sync_thread, args=(parent, sendq, data))
    sync_proc.start()

    while data.alive:
        msg = sendq.get()  # blocks
        if msg is None:
            continue

        store, msg = msg
        _debug("MSG %r %s" % (store, msg))

        try:
            sock.sendall(msg.encode('ascii', 'strict') + b"\n")
            # print(hilite("<<< %s" % msg, False))
            _debug("< %s" % msg)

            buff = []
            while True:
                recd = sock.recv(512).decode('ascii', 'strict')
                if not recd:
                    print(hilite("Peer disconnected", False, bold=True))
                    break

                buff.append(recd)
                if '\n' in recd:
                    break

            read = ''.join(buff)[:-1]
            _debug("> %s" % read)
            sendq.task_done()

            if store:
                recq.put_nowait((True, read))

            # print(hilite(">>> %s" % read, True))

        except socket.error:
            parent._error("Socket is unavailable")
        except EOFError:
            pass

    _debug("LOL ACHAO PLZ")
    try:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
    except socket.error as e:
        _debug("WAT ERROR??? %s" % e)
        pass

    data.alive = False
    data.announced = False
    parent._notify_disconnected()
    _debug("EXITED FROM THREAD LOOP")


class ClientPrompt(cmd.Cmd, object):
    def __init__(self):
        super(ClientPrompt, self).__init__()
        self.prompt = PROMPT_DISCONNECTED
        self._addr = None
        self._listen_port = 80
        self._files = {}
        self._seeded = set()
        self._leeched = set()

    # def do_announce(self, args):
    #     """`announce` Announce on the tracker."""
    #     if self._listen_port is None:
    #         self._error("You must set the listen port with `listen`")
    #         return
    #     self._send_cmd("announce listen %d seed [] leech []" % self._listen_port)

    def do_connect(self, args):
        """`connect <host> <port>` Connect to tracker."""
        args = shlex.split(args)
        if mp_data.alive:
            self._error("Already connected")
            return

        try:
            host, port = args
            port = int(port)
            self._addr = (host, port)
            while not mp_queue_send.empty():
                mp_queue_send.get()
                mp_queue_send.task_done()

            sock_proc = mp.Process(target=socket_thread, args=(self, host, port, mp_queue_send, mp_queue_rec, mp_data))
            self._send_cmd("announce listen %d seed [%s] leech [%s]" % (
                self._listen_port,
                self._format_all_seeded("{name} {size} {key}"),
                self._format_all_leeched("{key}"),
            ))
            sock_proc.start()

            ok, msg = self._wait_answer()
            if ok:
                mp_data.announced = True
                self.prompt = PROMPT_CONNECTED % self._addr
            else:
                self._error("Unable to connect: %s" % msg)

        except Exception as e:
            traceback.print_exc()
            self._error(str(e))

    def do_disconnect(self, args):
        """`disconnect` Disconnect from the tracker."""
        if mp_data.alive:
            mp_data.alive = False
            mp_data.announced = False
            mp_queue_send.put(None)
        else:
            self._error("Not connected")

    def do_listen(self, args):
        """`listen <port>` Start listening on port."""
        try:
            self._listen_port = int(args)
            if not (1 <= self._listen_port < 1 << 16):
                raise ValueError
        except ValueError:
            self._error("<port> must be in range [1, %d]" % (1 << 16 - 1))
        except IndexError:
            self._error("<port> required")

    def do_listseed(self, args):
        for f in sorted(self._seeded):
            print(self._format_file(f))

    def do_quit(self, args):
        """`quit` Quits the program."""
        raise SystemExit

    def do_search(self, args):
        if not mp_data.alive:
            self._error("Not connected")
            return

        self._send_cmd("look [%s]" % args)
        ok, msg = self._wait_answer()
        parts = msg.strip('[]').split()
        for f in
            print(self._format_file(f))

    def do_seed(self, args):
        args = shlex.split(args)

        to_add = set()
        for f in args:
            if os.path.isfile(f):
                to_add.add(os.path.abspath(f))
            else:
                self._error("Unknown file: %s" % f)

        added = to_add - self._seeded.keys()
        for f in added:
            self._seeded[f] = (os.path.getsize(f), _filehash(f))

        print(hilite("added %d files; seeding a total of %d files" % (l, len(self._seeded)), True))
        self._notify_update()

    do_EOF = do_quit

    def precmd(self, line):
        if mp_data.alive:
            self.prompt = PROMPT_CONNECTED % self._addr
        else:
            self.prompt = PROMPT_DISCONNECTED
        return super(ClientPrompt, self).precmd(line)

    def completedefault(self, text, line, begidx, endidx):
        cmd = shlex.split(line)[0]
        if cmd == 'seed':
            # ls-like
            files = [f for f in os.listdir('.') if os.path.isfile(f) and f.startswith(text)]
            lf = len(files)
            if lf == 1:
                return [files[0]]
            elif lf > 1:
                return files

    def default(self, line):
        print("prompte is '%s'" % self.prompt)
        self._error("Unknown command: %s" % shlex.split(line)[0])

    def _wait_answer(self):
        return mp_queue_rec.get(block=True)

    def _send_cmd(self, cmd):
        mp_queue_send.put((True, cmd), block=True)

    def _notify_connected(self, host, port):
        # self.prompt = PROMPT_CONNECTED % (host, port)
        print(hilite("Connected to %s:%d" % (host,port), True, bold=True))

    def _notify_disconnected(self):
        print(hilite("Disconnected", False, bold=True))
        # self.prompt = PROMPT_DISCONNECTED
        # self.onecmd('')

    def _format_all_seeded(self, fmt):
        return " ".join(fmt.format(name=os.path.basename(f), size=v[0], key=v[1]) for f, v in self._seeded.iteritems())

    def _format_all_leeched(self, fmt):
        return " ".join(fmt.format(name=os.path.basename(f), size=v[0], key=v[1]) for f, v in self._leeched.iteritems())

    def _notify_update(self):
        with mp_lock:
            mp_data.seed = list(self._seeded.keys())
            mp_data.leech = list(self._leeched.keys())
            # mp_updated.set()

    def _error(self, msg):
        print(hilite("*** %s" % msg, success=False))

    def __del__(self):
        mp_data.alive = False
        mp_data.announced = False


if __name__ == '__main__':
    # import argparse
    # logging.basicConfig(level=logging.DEBUG, format='%(levelname)7s: %(message)s')

    prompt = ClientPrompt()
    prompt.cmdloop(hilite("Welcome to tracker client REPL.", bold=True))
