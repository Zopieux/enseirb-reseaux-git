#!/usr/bin/env python
# coding: utf-8
# Skeleton shamefully stolen from Doug Hellmann.

from __future__ import unicode_literals, print_function
import logging
import Queue
import select
import socket
import SocketServer
import sys
import pyparsing as pyp
import operator
from collections import defaultdict, deque

_logger = logging.getLogger(__name__)

BUFFER_SIZE = 1024
COMMAND_TERMINATOR = b'\n'
FIELD_SEPARATOR = ' '


def flatten(l):
    return [_[0] for _ in l]


def cmd_parser(fmt, **types):
    type_map = defaultdict(lambda: (lambda _: _))
    type_map.update(types)

    def _caster(tt):
        return  lambda s, l, t: type_map[tt](t[0])

    p_literal = pyp.Word(pyp.alphas)
    p_var = pyp.Word('$', pyp.alphas, min=2)
    p_list = pyp.Group(pyp.Suppress('[') + pyp.OneOrMore(p_var) + pyp.Suppress(']'))
    p_cmd = pyp.OneOrMore(p_literal | p_var | p_list) + pyp.StringEnd()

    elems = []

    for token in p_cmd.parseString(fmt.lower()).asList():
        if isinstance(token, list):
            li = [
                pyp.Regex(r'[^\s\[\]]+').setParseAction(_caster(subtoken[1:])).setName(subtoken[1:])
                for subtoken in token
            ]
            g = pyp.ZeroOrMore(pyp.Group(reduce(operator.__add__, li)))
            elems.append(pyp.Group(pyp.Suppress('[') + g + pyp.Suppress(']')))
        else:
            if token.startswith('$'):
                elems.append(pyp.Regex(r'[^\s\[\]]+').setParseAction(_caster(token[1:])).setName(token[1:]))
            else:
                elems.append(pyp.Suppress(pyp.Literal(token)))

    parser = pyp.Group(reduce(operator.__add__, elems) + pyp.StringEnd())

    return lambda raw: parser.parseString(raw)[0].asList()


class SeededFile(object):
    _all_keys = set()

    def __init__(self, name, key, length, piece_size):
        SeededFile._all_keys.add(key)
        self.name = name
        self.key = key
        self.length = length
        self.piece_size = piece_size


class TrackerClient(object):
    def __init__(self, listen_port, seed_dic, leech_set):
        self.listen_port = listen_port
        self.seed_dic = seed_dic
        self.leech_set = set(leech_set)

    @classmethod
    def from_announce(cls, rawstr):
        parsed = cmd_parser('listen $port seed [$fn $len $ps $key] leech [$key]', port=int, len=int, ps=int)
        port, seed, leech = parsed(rawstr)
        seed = {f[3]: SeededFile(*f) for f in seed}
        return cls(port, seed, flatten(leech))

    def update(self, seed_keys, leech_keys):
        new_keys = set(seed_keys)
        old_keys = set(self.seed_dic.keys())

        if new_keys - old_keys:
            raise ValueError("adding unknown keys through `update`")

        for useless in old_keys - new_keys:
            self.seed_dic.pop(useless)

        self.leech_set = set(leech_keys)

    def __repr__(self):
        return "<TrackerClient listening %d (%d seed, %d leech)>" % (
            self.listen_port, len(self.seed_dic), len(self.leech_set)
        )


class TCPHandler(SocketServer.StreamRequestHandler):

    def tracker_response(self, data):
        cmd = data.split(FIELD_SEPARATOR, 1)
        args = []
        raw_args = ''
        if len(cmd) == 1:
            cmd = cmd[0]
        else:
            cmd, raw_args = cmd
            args = raw_args.split(FIELD_SEPARATOR)

        if not self.client and cmd == 'announce':
            self.client = TrackerClient.from_announce(raw_args)
            _logger.info("%r: announced", self.client)
            return "ok"

        if not self.client:
            raise ValueError("not announced")

        if cmd == 'look':
            criterions = flatten(cmd_parser('[$crit]')(raw_args)[0])
            _logger.info("%r: criterions: %r", self.client, criterions)
            return "%r" % criterions

        if cmd == 'getfile':
            if len(args) != 1:
                raise ValueError("protocol error")
            fname = args[0]
            _logger.info("%r: getfile: %s", self.client, fname)
            return "peers %s []" % fname

        if cmd == 'update':
            seeds, leechs = map(flatten, cmd_parser('seed [$key] leech [$key]')(raw_args))
            self.client.update(seeds, leechs)
            _logger.info("%r: update: %r / %r", self.client, seeds, leechs)
            return "ok"

        raise ValueError("unkown command")

    def handle(self):
        self.connected = True
        self.client = None
        _logger.info("Handling new client: %r", self)

        while self.connected:
            data = self.rfile.readline().strip()
            _logger.debug('data in: %s', data)

            try:
                response = self.tracker_response(data)
            except pyp.ParseException as e:
                response = "USRERROR: %s (at col %d)" % (e.message, e.col)
            except Exception as e:
                error = e.message
                if not error:
                    error = repr(e)
                _logger.warning(error)
                response = "SRVERROR: %s" % error

            if not self.connected:
                break

            self.wfile.write(response + "\n")


class TrackerServer(SocketServer.TCPServer):
    def socket_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        super(TrackerServer, self).socket_bind()


if __name__ == '__main__':
    import argparse
    logging.basicConfig(level=logging.DEBUG, format='%(levelname)7s: %(message)s')

    parser = argparse.ArgumentParser(description="Fake Pirtoupire tracker.")
    parser.add_argument('-a', '--host', default='localhost', help="tracker listening host address")
    parser.add_argument('-p', '--port', default=6000, type=int, help="tracker listening port")

    args = parser.parse_args()

    server = TrackerServer((args.host, args.port), TCPHandler)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.server_close()
