#!/usr/bin/env python3
# coding: utf-8

from collections import defaultdict, deque
from redis import StrictRedis
from redis_collections import Dict
import functools
import logging
import operator
import pyparsing as pyp
import select
import signal
import socket
import socketserver
import sys
import threading

from criterion_matcher import criterion_matcher

_logger = logging.getLogger(__name__)
rclient = StrictRedis(db=2)

BUFFER_SIZE = 1024
MAX_LINE_LENGTH = BUFFER_SIZE * 2
COMMAND_TERMINATOR = b'\n'
FIELD_SEPARATOR = ' '

class ProtocolError(Exception): pass


def flatten(l):
    return [_[0] for _ in l]


def cmd_parser(fmt, **types):
    type_map = defaultdict(lambda: (lambda _: _))
    type_map.update(types)

    def sum(l):
        return functools.reduce(operator.add, l)

    def _caster(tt):
        return  lambda s, l, t: type_map[tt](t[0])

    p_literal = pyp.Word(pyp.alphas)
    p_var = pyp.Word('$', pyp.alphas, min=2)
    p_list = pyp.Group(pyp.Suppress('[') + pyp.OneOrMore(p_var) + pyp.Suppress(']'))
    p_cmd = pyp.OneOrMore(p_literal | p_var | p_list) + pyp.StringEnd()

    elems = []

    for token in p_cmd.parseString(fmt.lower()).asList():
        if isinstance(token, list):
            li = [
                pyp.Regex(r'[^\s\[\]]{1,256}').setParseAction(_caster(subtoken[1:])).setName(subtoken[1:])
                for subtoken in token
            ]
            g = pyp.ZeroOrMore(pyp.Group(sum(li)))
            elems.append(pyp.Group(pyp.Suppress('[') + g + pyp.Suppress(']')))
        else:
            if token.startswith('$'):
                elems.append(pyp.Regex(r'[^\s\[\]]{1,256}').setParseAction(_caster(token[1:])).setName(token[1:]))
            else:
                elems.append(pyp.Suppress(pyp.Literal(token)))

    parser = pyp.Group(sum(elems) + pyp.StringEnd())

    return lambda raw: parser.parseString(raw)[0].asList()


class SeededFile(object):
    _all_files = Dict(redis=rclient, key='enseirb-reseau-seeds')

    @classmethod
    def all_keys(cls):
        return set(_.decode('utf-8') for _ in cls._all_files.keys())

    @classmethod
    def all_files(cls):
        return cls._all_files.values()

    @classmethod
    def by_criterion(cls, criterion):
        matcher = criterion_matcher(criterion)
        return [file for key, file in cls._all_files.items() if matcher(file)]

    @classmethod
    def by_key(cls, key):
        return cls._all_files[key]

    @classmethod
    def register(cls, name, size, ps, key):
        assert isinstance(name, str) and isinstance(key, str)
        try:
            return cls.by_key(key)
        except KeyError:
            self = cls(name, size, ps, key)
            SeededFile._all_files[self.key] = self
            return self

    def __init__(self, name, size, ps, key):
        self.name = name
        self.key = key
        self.size = size
        self.piece_size = ps

    @property
    def proto_fmt(self):
        return "%s %d %d %s" % (self.name, self.size, self.piece_size, self.key)

    def __str__(self):
        return "[{key}] {fn} (piece: {ps} B, size: {size} B)".format(fn=self.name, size=self.size, ps=self.piece_size, key=self.key)


class TrackerConnection(socketserver.StreamRequestHandler):
    _cmd_announce = cmd_parser('listen $port seed [$fn $size $ps $key] leech [$key]', port=int, size=int, ps=int)
    _cmd_update = cmd_parser('seed [$key] leech [$key]')

    def is_seeding(self, key):
        return key in self.seed_set

    def is_leeching(self, key):
        return key in self.leech_set

    def tracker_response(self, data):
        self.stat_cmds += 1
        cmd = data.strip().lower().split(FIELD_SEPARATOR, 1)
        args = []
        raw_args = ''
        if len(cmd) == 1:
            cmd = cmd[0]
        else:
            cmd, raw_args = cmd
            args = raw_args.split(FIELD_SEPARATOR)

        try:
            method = getattr(self, 'handle_%s' % cmd)
            return method(raw_args, args)
        except AttributeError:
            raise ProtocolError("Unknown command")

    def handle_announce(self, raw, args):
        if self.announced:
            raise ProtocolError("Already announced")
        port, seed, leech = TrackerConnection._cmd_announce(raw)
        for co in TrackerServer.connections:
            if co != self and co.ipaddr == self.ipaddr and co.listen_port == port:
                raise ProtocolError("There is already a client listening to this (host, port)")
        if not 1 <= port < 1 << 16:
            raise ProtocolError("Invalid port")

        self.listen_port = port
        for fn, size, ps, key in seed:
            self.seed_set.add(key)
            SeededFile.register(fn, size, ps, key)
        self.leech_set = set(flatten(leech)).intersection(SeededFile.all_keys())
        _logger.debug("announce: %r", self)
        return "ok"

    def handle_look(self, raw, args):
        if not self.announced:
            raise ProtocolError("Not announced")
        criterion = raw.strip('[]').strip()
        if criterion:
            results = SeededFile.by_criterion(criterion)
        else:
            results = SeededFile.all_files()
        return "list [%s]" % ' '.join(r.proto_fmt for r in results)

    def handle_getfile(self, raw, args):
        if not self.announced:
            raise ProtocolError("Not announced")
        if len(args) != 1:
            raise ProtocolError("Expected parameter")
        key = args[0]
        peers = TrackerServer.seeding_peers(key)
        return "peers %s [%s]" % (key, ' '.join(p.proto_fmt for p in peers))

    def handle_update(self, raw, args):
        if not self.announced:
            raise ProtocolError("Not announced")
        seeds, leechs = map(flatten, TrackerConnection._cmd_update(raw))
        all_keys = SeededFile.all_keys()
        _logger.debug("update? %r %r", seeds, leechs)
        _logger.debug("all keys are: %r", all_keys)
        self.seed_set = set(seeds).intersection(all_keys)
        self.leech_set = set(leechs).intersection(all_keys)
        _logger.debug("updated: %r", self)
        return "ok"

    def setup(self):
        super().setup()
        self.stat_cmds = 0
        self.alive = True
        self.listen_port = None
        self.seed_set = set()
        self.leech_set = set()

        _logger.info("connected: %s", self.client_address)
        with TrackerServer.conn_lock:
            TrackerServer.connections.append(self)

    def handle(self):
        while self.alive:
            data = self.rfile.readline(MAX_LINE_LENGTH).decode('ascii', 'ignore').strip()
            if not data:
                break
            _logger.debug(">> %s", data)

            try:
                response = self.tracker_response(data)
            except pyp.ParseException as e:
                response = "ERROR  PARSE: at col %d: %s" % (e.col, e.msg)
            except ProtocolError as e:
                response = "ERROR  PROTO: %s" % e
            except Exception as e:
                _logger.warning(e)
                response = "ERROR SERVER: %s" % e

            _logger.debug("<< %s", response)
            self.wfile.write((response + "\n").encode('ascii', 'replace'))

        self.alive = False

    def finish(self):
        super().finish()
        self.alive = False
        _logger.info("disconnected: %s", self.client_address)
        with TrackerServer.conn_lock:
            TrackerServer.connections.remove(self)

    @property
    def announced(self):
        return self.listen_port is not None

    @property
    def ipaddr(self):
        return self.client_address[0]

    @property
    def proto_fmt(self):
        return "%s:%r" % (self.ipaddr, self.listen_port)

    def __repr__(self):
        return "<Client %s seeding[%r] leeching[%r]>" % (
            self.proto_fmt, self.seed_set, self.leech_set
        )


class TrackerServer(socketserver.ThreadingTCPServer):
    allow_reuse_address = True
    conn_lock = threading.Lock()
    connections = []

    def exit(self, *args):
        for c in TrackerServer.connections:
            c.connection.shutdown(socket.SHUT_RDWR)
            c.connection.close()
            self.shutdown_request(c.request)

        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()

    @classmethod
    def seeding_peers(cls, key):
        return [c for c in cls.connections if c.is_seeding(key)]


if __name__ == '__main__':
    import argparse
    logging.basicConfig(level=logging.DEBUG, format='%(levelname)7s: %(message)s', filename='/tmp/eirb-tracker.log')

    parser = argparse.ArgumentParser(description="Fake Pirtoupire tracker.")
    parser.add_argument('-a', '--host', default='localhost', help="tracker listening host address")
    parser.add_argument('-p', '--port', default=6000, type=int, help="tracker listening port")

    args = parser.parse_args()

    server = TrackerServer((args.host, args.port), TrackerConnection)

    signal.signal(signal.SIGINT, server.exit)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass

