package fr.enseirb.pirtoupire;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * Static container for global configuration variables. See assets/default.conf.ini for an explanation of each property.
 */
public final class Configuration {

	public static File SESSION_DIRECTORY;
	public static InetSocketAddress PEERLISTENER_ADDR;
	public static int PEERLISTENER_MAX_PEERS;
	public static int PROTOCOL_MAX_LENGTH;
	public static int PROTOCOL_MAX_PEER_PIECES;
	public static int PROTOCOL_UPDATE_TRACKER;
	public static int PROTOCOL_UPDATE_PEERS;
	public static int PROTOCOL_UPDATE_NEW_PEERS;
	public static boolean HARDCODE_LOCALHOST;

	public static Pattern PROTOCOL_RESERVED_PATTERN = Pattern.compile("[\\s\\[\\]]");

	private final static String PREFIX = "pirtoupire";

	/**
	 * Load the configuration properties from the given path.
	 * 
	 * @param path
	 * @throws IOException
	 */
	public static void loadConfiguration(File path) throws IOException {
		Properties prop = new Properties();
		InputStream is = new FileInputStream(path);
		prop.load(is);
		is.close();

		Configuration.SESSION_DIRECTORY = new File(prop.getProperty(PREFIX + ".session.directory"));
		String host = prop.getProperty(PREFIX + ".peerlistener.host");
		int port = Integer.valueOf(prop.getProperty(PREFIX + ".peerlistener.port"));
		Configuration.PEERLISTENER_ADDR = new InetSocketAddress(host, port);
		Configuration.PEERLISTENER_MAX_PEERS = Integer.valueOf(prop.getProperty(PREFIX + ".peerlistener.max_peers"));
		Configuration.PROTOCOL_MAX_LENGTH = Integer.valueOf(prop.getProperty(PREFIX + ".protocol.max_length"));
		Configuration.PROTOCOL_MAX_PEER_PIECES = Integer
				.valueOf(prop.getProperty(PREFIX + ".protocol.max_peer_pieces"));
		Configuration.PROTOCOL_UPDATE_TRACKER = Integer.valueOf(prop.getProperty(PREFIX
				+ ".protocol.update_interval.tracker"));
		Configuration.PROTOCOL_UPDATE_PEERS = Integer.valueOf(prop.getProperty(PREFIX
				+ ".protocol.update_interval.peers"));
		Configuration.PROTOCOL_UPDATE_NEW_PEERS = Integer.valueOf(prop.getProperty(PREFIX
				+ ".protocol.update_interval.new_peers"));
		Configuration.HARDCODE_LOCALHOST = Boolean.valueOf(prop.getProperty(PREFIX
				+ ".hardcode_localhost"));

		System.err.println("Loaded configuration from file " + path);
		Configuration.SESSION_DIRECTORY.mkdirs();
		System.err.println("Created session directory at "
				+ Configuration.SESSION_DIRECTORY.getCanonicalPath());
	}

	public static String toStaticString() {
		StringBuilder sb = new StringBuilder("Configuration {\n");
		sb.append("\t      SESSION_DIRECTORY: ").append(Configuration.SESSION_DIRECTORY).append("\n");
		sb.append("\t      PEERLISTENER_ADDR: ").append(Configuration.PEERLISTENER_ADDR).append("\n");
		sb.append("\t PEERLISTENER_MAX_PEERS: ").append(Configuration.PEERLISTENER_MAX_PEERS).append("\n");
		sb.append("\t    PROTOCOL_MAX_LENGTH: ").append(Configuration.PROTOCOL_MAX_LENGTH).append("\n");
		sb.append("\t  PROTOCOL_MAX_P_PIECES: ").append(Configuration.PROTOCOL_MAX_PEER_PIECES).append("\n");
		sb.append("\tPROTOCOL_UPDATE_TRACKER: ").append(Configuration.PROTOCOL_UPDATE_TRACKER).append("\n");
		sb.append("\t  PROTOCOL_UPDATE_PEERS: ").append(Configuration.PROTOCOL_UPDATE_PEERS).append("\n");
		sb.append("\t PROTOCOL_UPDATE_NPEERS: ").append(Configuration.PROTOCOL_UPDATE_NEW_PEERS).append("\n");
		sb.append("\t     HARDCODE_LOCALHOST: ").append(Configuration.HARDCODE_LOCALHOST).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
