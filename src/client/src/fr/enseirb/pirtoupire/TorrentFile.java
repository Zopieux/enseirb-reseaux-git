package fr.enseirb.pirtoupire;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import fr.enseirb.pirtoupire.util.Utils;

/**
 * Represents a torrent file as returned by the tracker. This class has no internal logic, it only provides utility
 * methods to compute some useful attributes such as the number of pieces.
 */
/**
 * 
 */
public class TorrentFile implements Comparable<TorrentFile> {

	private String fileName; // can be arbitrary, not necessarily the same as on disk
	private String fileKey; // can be arbitrary
	private final Long fileLength;
	private Integer pieceLength;

	public TorrentFile(File file) {
		// compute file hash as key
		this(file, Utils.computeFileHash(file));
	}

	public TorrentFile(File file, String key) {
		// use default piece length
		// this(file, key, Configuration.PROTOCOL_DEFAULT_PIECE_LENGTH);
		// compute best piece length
		this(file, key, TorrentFile.computeOptimalPieceSize(file));
	}

	public TorrentFile(File file, int piecelength) {
		this(file);
		this.pieceLength = piecelength;
	}

	public TorrentFile(File file, String key, int piecelength) {
		this(file.getName(), key, file.length(), piecelength);
	}

	public TorrentFile(String name, String key, Long length, int piecelength) {
		fileLength = length;
		pieceLength = piecelength;
		setKey(key);
		setName(name);
		assert getPieceCount() >= 1;
	}

	/**
	 * The name of the torrent as set by the primary seeder. May be different than the on-disk name.
	 * 
	 * @return torrent name
	 */
	public String getName() {
		return fileName;
	}

	public void setName(String name) {
		if (Configuration.PROTOCOL_RESERVED_PATTERN.matcher(name).matches()) {
			throw new IllegalArgumentException("Torrent names can not contain white spaces");
		}
		fileName = name;
	}

	/**
	 * The torrent file length as set by the primary seeder.
	 * 
	 * @return torrent file length
	 */
	public Long getLength() {
		return fileLength;
	}

	/**
	 * The length of each piece as set by the primary seeder. The last piece may be smaller. Always use
	 * `getPieceLength(int)` for this purpose.
	 * 
	 * @return the main piece length
	 */
	public int getPieceLength() {
		return pieceLength;
	}

	public void validatePieceIndex(int pieceIndex) {
		if (!(0 <= pieceIndex && pieceIndex < getPieceCount())) {
			throw new ArrayIndexOutOfBoundsException("Invalid piece index: " + pieceIndex);
		}
	}

	/**
	 * The length of a given piece (see `getPieceLength()`).
	 * 
	 * @param pieceIndex
	 *            the index of the piece, between 0 inclusive and `getPieceCount()` exclusive
	 * @return the piece length of the given piece
	 */
	public int getPieceLength(int pieceIndex) {
		validatePieceIndex(pieceIndex);
		int pcMinusOne = getPieceCount() - 1;
		if (pieceIndex == pcMinusOne) {
			return new Long(getLength() - new Long(pcMinusOne * getPieceLength())).intValue();
		} else {
			return getPieceLength();
		}
	}

	/**
	 * The maximum amount of pieces that can be requested in the same `GETPIECES` request.
	 * 
	 * @return the maximum amount of pieces
	 */
	public int getMaxNumberRequestablePieces() {
		return Math.max(1, Configuration.PROTOCOL_MAX_LENGTH / getPieceLength() - 1);
	}

	/**
	 * The torrent key, as set by the primary seeder.
	 * 
	 * @return the torrent key
	 */
	public String getKey() {
		return fileKey;
	}

	public void setKey(String key) {
		if (Configuration.PROTOCOL_RESERVED_PATTERN.matcher(key).matches()) {
			throw new IllegalArgumentException("Torrent keys can not contain white spaces");
		}
		fileKey = key;
	}

	/**
	 * Computes the torrent's unique signature, a hash comprised of every piece of info (name, length, piece size, key).
	 * 
	 * @return the torrent's unique signature
	 */
	public String getSignature() {
		// TODO: cache this
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			digest.update(fileName.getBytes("UTF-8"));
			digest.update(fileKey.getBytes("UTF-8"));
			digest.update(fileLength.toString().getBytes("UTF-8"));
			digest.update(pieceLength.toString().getBytes("UTF-8"));
			return Utils.byteArrayToHexString(digest.digest());
		} catch (NoSuchAlgorithmException ex) {
			throw new InternalError(ex.toString());
		} catch (IOException ex) {
			throw new InternalError(ex.toString());
		}
	}

	/**
	 * The number of pieces for this torrent.
	 * 
	 * @return
	 */
	public Integer getPieceCount() {
		return new BigDecimal(getLength()).divide(
				new BigDecimal(getPieceLength()), RoundingMode.UP).intValue();
	}

	/**
	 * The protocol representation for this torrent, as used by the tracker.
	 * 
	 * @return
	 */
	public String toTrackerFormat() {
		return Utils.implode(" ", getName(), String.valueOf(getLength()),
				String.valueOf(getPieceLength()), getKey());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("TorrentFile {\n");
		sb.append("\t        Name: ").append(getName()).append("\n");
		sb.append("\t         Key: ").append(getKey()).append("\n");
		sb.append("\t      Length: ").append(getLength()).append("\n");
		sb.append("\tPiece length: ").append(getPieceLength()).append("\n");
		sb.append("\t Piece count: ").append(getPieceCount()).append("\n");
		sb.append("\t   Signature: ").append(getSignature()).append("\n");
		sb.append("}");
		return sb.toString();
	}

	@Override
	public int compareTo(TorrentFile o) {
		// return getSignature().compareTo(o.getSignature());
		return getKey().compareTo(o.getKey());
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof TorrentFile) {
			return getKey().equals(((TorrentFile) o).getKey());
		}
		return false;
	}

	private static int computeOptimalPieceSize(File file) {
		// max size is 1MB
		final int max_ps = 1 << 20;
		int ps = max_ps;
		while (ps > max_ps || ps >= Configuration.PROTOCOL_MAX_LENGTH / 2) {
			ps /= 2;
		}
		return ps;
	}
}
