package fr.enseirb.pirtoupire.centralized;


public abstract class AbstractMessage {

	public abstract String getStringRepr();

	public static class ProtocolException extends IllegalStateException {
		private static final long serialVersionUID = 6742527023250180000L;

		public ProtocolException(String msg) {
			super(msg);
		}

		public ProtocolException(String msg, Exception reason) {
			super(msg, reason);
		}

		public ProtocolException(Exception reason) {
			super(reason);
		}
	}

}
