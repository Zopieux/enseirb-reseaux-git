package fr.enseirb.pirtoupire.centralized;

import java.io.File;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.JFrame;

import fr.enseirb.pirtoupire.Configuration;
import fr.enseirb.pirtoupire.ui.Screen;
import fr.enseirb.pirtoupire.util.FinderTorrentFile;
import fr.enseirb.pirtoupire.util.Utils;

public class Client {
	private final static String HELP_USAGE = "Usage: <tracker host> <tracker port>";

	public static void main(String[] args) throws Exception {
		// prevents using IPv6 because it makes another inetaddress
		System.setProperty("java.net.preferIPv4Stack", "true");

		String allArgs = Utils.implode(" ", args);
		Scanner ascan = new Scanner(allArgs);
		ascan.findInLine("^(\\-c|\\-\\-conf)\\s*([^\\s]+)");
		File confPath;
		try {
			confPath = new File(ascan.match().group(2));
		} catch (IllegalStateException e) {
			System.out.println("Using default configuration file");
			confPath = new File("conf/default.ini");
		}

		InetAddress tracker_host = null;
		int tracker_port = 0;

		try {
			tracker_host = InetAddress.getByName(ascan.next("([\\w\\-\\._]+)"));
			tracker_port = ascan.nextShort();
		} catch (NoSuchElementException e) {
			System.err.println(HELP_USAGE);
			System.exit(1);
		} finally {
			ascan.close();
		}

		Configuration.loadConfiguration(confPath);
		System.out.println(Configuration.toStaticString());

		System.out.println("Connection.. addresse:" + tracker_host + "..port:"
				+ tracker_port);

		FinderTorrentFile.loadTorrentFile();
		// start peer acceptor
		Manager.startPeerAcceptor();
		// start tracker
		Manager.setTrackerAddress(new InetSocketAddress(tracker_host, tracker_port));
		Manager.startTrackerConnection();
		// start peers update thread
		Manager.startUpdate();
		Manager.startUpdateNewPeers();

		JFrame screen = new Screen();

	}
}