package fr.enseirb.pirtoupire.centralized;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import fr.enseirb.pirtoupire.Configuration;
import fr.enseirb.pirtoupire.TorrentFile;
import fr.enseirb.pirtoupire.io.FilesystemTorrent;
import fr.enseirb.pirtoupire.io.TorrentMetadata;

public final class Manager {
	private static PeerAcceptor peerAcceptor;
	private static TrackerConnection trackerConnection;
	private static InetSocketAddress trackerAddr;
	private static Map<InetSocketAddress, Peer> addrToPeer = Collections
			.synchronizedMap(new HashMap<InetSocketAddress, Peer>());
	// key to TorrentFile
	private static Map<String, TorrentFile> seenTorrents = Collections
			.synchronizedMap(new HashMap<String, TorrentFile>());
	private static Map<TorrentFile, Torrent> activeTorrents = Collections
			.synchronizedMap(new HashMap<TorrentFile, Torrent>());
	private static final ActiveTorrentObservable observable = new ActiveTorrentObservable();

	private static final Logger logger = Logger.getLogger(Manager.class.getName());

	public static void setTrackerAddress(InetSocketAddress trackerAddr) {
		Manager.trackerAddr = trackerAddr;
	}

	public static synchronized FilesystemTorrent leechNewTorrent(TorrentFile torrentFile, File destinationFile) throws IOException {
		if (activeTorrents.containsKey(torrentFile)) {
			// already leeching
			return activeTorrents.get(torrentFile).getFilesystemTorrent();
		}
		FilesystemTorrent fsTorrent;
		fsTorrent = new FilesystemTorrent(torrentFile, destinationFile, false);
		Torrent t = new Torrent(fsTorrent, true); // we are nice and we seed it
		seenTorrents.put(torrentFile.getKey(), torrentFile);
		activeTorrents.put(torrentFile, t);
		observable.updated();
		return fsTorrent;
	}

	public static synchronized FilesystemTorrent newTorrentFromMetadata(TorrentMetadata meta) throws IOException {
		FilesystemTorrent fsTorrent = meta.getFilesystemTorrent();
		TorrentFile torrentFile = fsTorrent.getTorrentFile();
		if (activeTorrents.containsKey(torrentFile)) {
			// already leeching
			return activeTorrents.get(torrentFile).getFilesystemTorrent();
		}
		Torrent t = new Torrent(fsTorrent, true); // we are nice and we seed it
		seenTorrents.put(torrentFile.getKey(), torrentFile);
		activeTorrents.put(torrentFile, t);
		observable.updated();
		return fsTorrent;
	}

	public static synchronized FilesystemTorrent seedNewTorrent(TorrentFile torrentFile, File pathToFile) throws IOException {
		if (activeTorrents.containsKey(torrentFile)) {
			// already leeching
			return activeTorrents.get(torrentFile).getFilesystemTorrent();
		}
		FilesystemTorrent fsTorrent;
		fsTorrent = new FilesystemTorrent(torrentFile, pathToFile, true);
		Torrent t = new Torrent(fsTorrent, true);
		seenTorrents.put(torrentFile.getKey(), fsTorrent.getTorrentFile());
		activeTorrents.put(torrentFile, t);
		observable.updated();
		// reset tracker connection to announce the new file (this is so stupid)
		startTrackerConnection();
		return fsTorrent;
	}

	public static Set<TorrentFile> getSeededTorrents() {
		Set<TorrentFile> seeded = new HashSet<TorrentFile>();
		synchronized (activeTorrents) {
			for (Torrent t : activeTorrents.values()) {
				if (t.isSeeding()) {
					seeded.add(t.getFileTorrent());
				}
			}
		}
		return seeded;
	}

	public static Set<TorrentFile> getLeechedTorrents() {
		Set<TorrentFile> leeched = new HashSet<TorrentFile>();
		synchronized (activeTorrents) {
			for (Torrent t : activeTorrents.values()) {
				if (t.isLeeching()) {
					leeched.add(t.getFileTorrent());
				}
			}
		}
		return leeched;
	}

	public static synchronized void registerPeer(InetSocketAddress addr, Peer peer) {
		if (addrToPeer.containsKey(addr)) {
			logger.warning("Peer " + peer + " already registered at " + addr);
			// TODO: disconnect from old peer?
		}
		addrToPeer.put(addr, peer);
		logger.fine("There are " + addrToPeer.size() + " registered peers");
	}

	public static synchronized void unregisterPeer(InetSocketAddress addr) {
		Peer p = addrToPeer.get(addr);
		addrToPeer.remove(addr);
		if (p != null) {
			for (Torrent t : activeTorrents.values()) {
				t.eventPeerDisconnected(p);
			}
		}
		logger.fine("There are " + addrToPeer.size() + " registered peers");
	}

	public static synchronized TorrentFile torrentFileFromKey(String key) {
		return seenTorrents.get(key);
	}

	public static synchronized Torrent getActiveTorrent(TorrentFile tf) {
		return activeTorrents.get(tf);
	}

	public static Peer connectToPeer(InetSocketAddress addr) {
		synchronized (addrToPeer) {
			if (addrToPeer.containsKey(addr)) {
				// if we already know this peer, return it instead of launching a new socket
				return addrToPeer.get(addr);
			}
		}
		Peer peer = null;
		try {
			logger.info("Connecting to peer at " + addr);
			SocketChannel channel = SocketChannel.open();
			channel.connect(addr);
			peer = new Peer(addr, channel);
			logger.info("Connected to new peer: " + peer);
		} catch (UnknownHostException e) {
			throw new RuntimeException("Unable to connect to peer " + addr, e);
		} catch (IOException e) {
			throw new RuntimeException("Unable to connect to peer " + addr, e);
		}
		registerPeer(addr, peer);
		return peer;
	}

	public static Set<TorrentFile> trackerLook(String criterions) {
		if (trackerConnection == null) {
			throw new RuntimeException("not connected to tracker");
		}
		return trackerConnection.look(criterions);
	}

	public static void startPeerAcceptor() {
		try {
			peerAcceptor = new PeerAcceptor(Configuration.PEERLISTENER_ADDR);
		} catch (IOException e) {
			throw new RuntimeException("Cannot start peer acceptor", e);
		}
		peerAcceptor.start();
		logger.info("PeerAcceptor started");
	}

	public static void startTrackerConnection() {
		if (trackerConnection != null && trackerConnection.isAlive()) {
			trackerConnection.close();
		}
		try {
			trackerConnection = new TrackerConnection(trackerAddr);
		} catch (IOException e) {
			throw new RuntimeException("Cannot connect to tracker", e);
		}
		trackerConnection.announce(Configuration.PEERLISTENER_ADDR.getPort(),
				getSeededTorrents(), getLeechedTorrents());
		logger.info("Announced to tracker");
		trackerConnection.startUpdater();
	}

	/**
	 * For each active torrent, check if it is still active. If so, check if we can request some pieces from others
	 * peers.
	 */
	public static void startUpdate() {
		Timer peers_update = new Timer();
		peers_update.scheduleAtFixedRate(new TimerTask() {
			@Override
			public synchronized void run() {
				for (Iterator<Map.Entry<TorrentFile, Torrent>> i = activeTorrents.entrySet().iterator(); i.hasNext();) {
					Map.Entry<TorrentFile, Torrent> at = i.next();
					Torrent t = at.getValue();
					logger.info("Torrent " + t + " updating");
					for (Peer p : t.getSeeders()) {
						// update for all seeders
						p.sendUpdate(t);
					}
					if (!t.isActive()) {
						// not needed anymore!
						t.stop();
						i.remove();
						observable.updated();
						logger.info("Torrent " + t + " not active anymore");
						continue;
					}
				}
			}
		}, Configuration.PROTOCOL_UPDATE_PEERS * 1000l, Configuration.PROTOCOL_UPDATE_PEERS * 1000l);
	}

	/**
	 * Regularly ask the tracker for the peers that have the torrents we need to leech
	 */
	public static void startUpdateNewPeers() {
		Timer new_peers_update = new Timer();
		new_peers_update.scheduleAtFixedRate(new TimerTask() {
			@Override
			public synchronized void run() {
				for (Torrent t : activeTorrents.values()) {
					if (t.isLeeching()) {
						Set<InetSocketAddress> peerlist = trackerConnection.getPeersForTorrentFile(t.getFileTorrent());
						for (InetSocketAddress addr : peerlist) {
							if (Configuration.HARDCODE_LOCALHOST) {
								// hardcode localhost for debug purposes
								addr = new InetSocketAddress("localhost", addr.getPort());
							}
							InetSocketAddress self = peerAcceptor.getListenAddress();
							if (addr.getAddress().getCanonicalHostName()
									.equals(self.getAddress().getCanonicalHostName())
									&& addr.getPort() == self.getPort()) {
								continue;
							}
							logger.fine("Found seeding peer " + addr);
							final InetSocketAddress peer_addr = addr;
							final Torrent torrent = t;
							Thread pt = new Thread(new Runnable() {
								@Override
								public void run() {
									Peer peer = Manager.connectToPeer(peer_addr);
									logger.info("Requesting torrent " + torrent + " from " + peer);
									peer.requestTorrent(torrent.getFileTorrent());
								}
							});
							pt.start();
						}
					}
				}
			}
		}, Configuration.PROTOCOL_UPDATE_NEW_PEERS * 1000l, Configuration.PROTOCOL_UPDATE_NEW_PEERS * 1000l);
	}

	public static synchronized Set<Torrent> getActiveTorrents() {
		return new HashSet<Torrent>(activeTorrents.values());
	}

	public static TrackerConnection getTrackerConnection() {
		return trackerConnection;
	}

	public static ActiveTorrentObservable getObservable() {
		return observable;
	}

	public static class ActiveTorrentObservable extends Observable {
		public void updated() {
			setChanged();
			notifyObservers();
		}
	}
}
