package fr.enseirb.pirtoupire.centralized;

import java.io.EOFException;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import fr.enseirb.pirtoupire.TorrentFile;
import fr.enseirb.pirtoupire.centralized.AbstractMessage.ProtocolException;
import fr.enseirb.pirtoupire.util.BufferMap;

public class Peer implements Comparable<Peer> {
	private final InetSocketAddress addr;
	private final SocketChannel channel;
	private final BlockingQueue<PeerMessage> sendQueue;
	private final Thread inThread;
	private final Thread outThread;
	private boolean stop;

	public Peer(InetSocketAddress addr, SocketChannel channel) {
		this.addr = addr;
		this.channel = channel;
		this.sendQueue = new LinkedBlockingQueue<PeerMessage>();
		this.stop = false;
		inThread = new Thread(new IncomingHandler(this));
		inThread.start();
		outThread = new Thread(new OutgoingHandler(this));
		outThread.start();
	}

	public InetSocketAddress getAddress() {
		return addr;
	}

	public void abort() {
		if (stop) {
			return;
		}
		stop = true;
		inThread.interrupt();
		outThread.interrupt();
		if (channel.isConnected()) {
			try {
				channel.close();
			} catch (IOException e) {
			}
		}
		Manager.unregisterPeer(addr);
		System.out.println("Aborted connection with " + this);
	}

	@Override
	public int compareTo(Peer o) {
		// FIXME: enough?
		return addr.toString().compareTo(o.toString());
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Peer) {
			return getAddress().getAddress().equals(((Peer) o).getAddress().getAddress());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return addr.getAddress().hashCode();
	}

	@Override
	public String toString() {
		return "<Peer at " + addr + ">";
	}

	public void sendMessage(PeerMessage message) {
		try {
			sendQueue.put(message);
		} catch (InterruptedException e) {
			System.out.println("Unable to add to sendQueue");
		}
	}

	public void requestTorrent(TorrentFile torrentFile) {
		PeerMessage.InterestedMessage msg = PeerMessage.InterestedMessage.craft(torrentFile);
		sendMessage(msg);
	}

	public void requestPieces(Torrent torrent, Set<Integer> pieces) {
		torrent.addRequestedPieces(pieces, this);
		PeerMessage.GetpiecesMessage msg = PeerMessage.GetpiecesMessage
				.craft(torrent.getFileTorrent().getKey(), pieces);
		sendMessage(msg);
	}

	public void sendUpdate(Torrent torrent) {
		// synchronized (torrent) {
		sendMessage(PeerMessage.HaveMessage.craft(torrent.getFileTorrent().getKey(), torrent.getFilesystemTorrent()
				.getMetadata().getCompletedBufferMap()));
		// }
	}

	public void handleMessage(PeerMessage message) {
		System.out.println("Recieved message from " + this + ": " + message);
		switch (message.getType()) {
		case INTERESTED: {
			PeerMessage.InterestedMessage msg = (PeerMessage.InterestedMessage) message;
			TorrentFile tf = Manager.torrentFileFromKey(msg.getKey());
			if (tf == null) {
				// we don't even know this torrent, answer we have no piece
				// System.out.println("Recevied INTERSTED but torrentFILE unknown! Sending 0 buffermap");
				// sendMessage(PeerMessage.HaveMessage.craft(msg.getKey(), new BufferMap(1)));
			} else {
				Torrent t = Manager.getActiveTorrent(tf);
				if (t != null) {
					sendMessage(t.eventPeerInterested(this));
				} else {
					System.out.println("Recevied INTERSTED but torrent unknown! Sending 0000 buffermap");
					sendMessage(PeerMessage.HaveMessage.craft(msg.getKey(), new BufferMap(tf.getPieceCount())));
				}
			}
			break;
		}
		case DATA: {
			PeerMessage.DataMessage msg = (PeerMessage.DataMessage) message;
			TorrentFile tf = Manager.torrentFileFromKey(msg.getKey());
			if (tf == null) {
				// we do not know this torrent!
				System.out.println("Received data for unknown torrent");
			} else {
				Torrent t = Manager.getActiveTorrent(tf);
				if (t == null || !t.isLeeching()) {
					System.out.println("Received data for inactive torrent");
				} else {
					System.out.println("OMG I WROTE THE SAID DATA");
					t.eventPeerPieceData(this, msg.getPieces());
				}
			}
			break;
		}
		case GETPIECES: {
			PeerMessage.GetpiecesMessage msg = (PeerMessage.GetpiecesMessage) message;
			TorrentFile tf = Manager.torrentFileFromKey(msg.getKey());
			Torrent t;
			if (tf != null && (t = Manager.getActiveTorrent(tf)) != null) {
				if (t == null || !t.isSeeding()) {
					System.out.println("Received data for inactive torrent");
				} else {
					Map<Integer, byte[]> pieces = new HashMap<Integer, byte[]>();
					for (int p : msg.getPieces()) {
						try {
							pieces.put(p, t.getFilesystemTorrent().read(p));
						} catch (IOException e) {
							System.out.println("Cannot read piece " + p + " of torrent " + t.getFilesystemTorrent());
						}
					}
					sendMessage(PeerMessage.DataMessage.craft(tf.getKey(), pieces));
				}
			} else {
				System.out.println("unknown or inactive torrent");
			}
			break;
		}
		case HAVE: {
			PeerMessage.HaveMessage msg = (PeerMessage.HaveMessage) message;
			TorrentFile tf = Manager.torrentFileFromKey(msg.getKey());
			Torrent t;
			if (tf != null && (t = Manager.getActiveTorrent(tf)) != null) {
				t.eventPeerHave(this, msg.getBufferMap());
			} else {
				System.out.println("unknown or inactive torrent");
			}
			break;
		}
		}
	}

	private class IncomingHandler implements Runnable {
		private final Peer peer;

		public IncomingHandler(Peer peer) {
			this.peer = peer;
		}

		@Override
		public void run() {
			try {
				System.out.println("CONNECTING TO PEER " + peer);
				PushbackInputStream in = new PushbackInputStream(Channels.newInputStream(channel),
						PeerMessage.PREFIX_SIZE);
				while (!stop) {
					System.out.println("Reading from peer (blocking)");
					try {
						PeerMessage message = PeerMessage.parse(in, peer);
						handleMessage(message);
					} catch (IOException e) {
						throw new RuntimeException("Could not parse peer message (IO error)", e);
					} catch (ProtocolException e) {
						throw new RuntimeException("Could not parse peer message (Protocol error)", e);
					}
				}
			} catch (NullPointerException e) {
				throw new RuntimeException("Null pointer while reading from peer", e);
			} finally {
				// if we exited the loop we must disconnect
				abort();
			}
		}
	}

	private class OutgoingHandler implements Runnable {
		private final Peer peer;

		public OutgoingHandler(Peer peer) {
			this.peer = peer;
		}

		@Override
		public void run() {
			try {
				while (!stop || (stop && sendQueue.size() > 0)) {
					try {
						PeerMessage message = sendQueue.take();
						if (stop) {
							return;
						}
						System.out.println("Sending message to " + peer + ": " + message);
						ByteBuffer data = message.getData();
						while (!stop && data.hasRemaining()) {
							if (channel.write(data) < 0) {
								throw new EOFException("Reached EOF while sending to " + peer);
							}
						}
						System.out.println("Wrote");
					} catch (InterruptedException e) {
						// ignored
					}
				}
			} catch (IOException e2) {
				throw new RuntimeException("Unable to send to peer " + peer, e2);
			} finally {
				// if we exited the loop we must disconnect
				abort();
			}
		}
	}
}
