package fr.enseirb.pirtoupire.centralized;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PeerAcceptor implements Runnable {

	private final InetSocketAddress listen_addr;
	// private ServerSocket bindSocket;
	private ServerSocketChannel channel;
	private ExecutorService executor;
	private Thread thread;
	private boolean stop;

	/**
	 * A little useless class for ThreadPoolExecutor.
	 */
	private static class ConnectorThreadFactory implements ThreadFactory {
		private int uid = 0;

		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r);
			t.setName("pirtoupire-" + (++this.uid));
			return t;
		}
	};

	/**
	 * Executor task to handle the remote connection to some peer.
	 */
	private static class PeerConnectionTask implements Runnable {

		private final Peer peer;

		public PeerConnectionTask(PeerAcceptor acceptor, Peer peer) {
			this.peer = peer;
		}

		@Override
		public void run() {
			InetSocketAddress addr = peer.getAddress();
			SocketChannel channel = null;
			try {
				// connect!
				channel = SocketChannel.open(addr);
				while (!channel.isConnected()) {
					Thread.sleep(100);
				}
				// channel.configureBlocking(false);
				System.out.println("Connected to remote " + peer);
				Manager.registerPeer(addr, peer);
				// TODO: send interested
			} catch (Exception e) {
				System.out.println("Unable to connect to remote " + peer);
				try {
					// cleanup
					if (channel != null && channel.isConnected()) {
						channel.close();
					}
				} catch (IOException e2) {

				}
				// peer.abort();
				Manager.unregisterPeer(addr);
				// TODO: notify failure
			}
		}
	}

	public PeerAcceptor(InetSocketAddress listen_addr) throws IOException {
		this.listen_addr = listen_addr;
		try {
			channel = ServerSocketChannel.open();
			// channel.configureBlocking(false);
			channel.socket().bind(listen_addr);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (channel == null || !channel.socket().isBound()) {
			throw new IOException("Unable to bind to " + listen_addr);
		}
		executor = null;
		thread = null;
	}

	public void start() {
		stop = false;
		if (executor == null || executor.isShutdown()) {
			// instantiate new executor
			executor = new ThreadPoolExecutor(5, 5, 10, TimeUnit.SECONDS,
					new LinkedBlockingQueue<Runnable>(), new ConnectorThreadFactory());
		}
		if (thread == null || !thread.isAlive()) {
			thread = new Thread(this);
			thread.setName("pirtoupire peer acceptor");
			thread.start();
		}
	}

	public void stop() {
		stop = true;
		if (thread != null && thread.isAlive()) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
		if (executor != null && !executor.isShutdown()) {
			executor.shutdownNow();
		}
		thread = null;
		executor = null;
	}

	public InetSocketAddress getListenAddress() {
		return listen_addr;
	}

	public boolean isRunning() {
		return executor != null && !executor.isShutdown() && !executor.isTerminated();
	}

	@Override
	public void run() {
		while (!stop) {
			try {
				SocketChannel client = channel.accept();
				if (client != null) {
					accept(client);
				}
			} catch (SocketTimeoutException e) {
				// ignore
			} catch (IOException e) {
				// something bad happened
				stop();
			}
			try {
				// give some time to be interrupted if needed
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
		// if exited, cleanup
		try {
			channel.close();
		} catch (IOException e) {

		}
	}

	public void accept(SocketChannel channel) throws IOException, SocketTimeoutException {
		try {
			System.out.println("New client! " + channel);
			// client.configureBlocking(false);
			channel.socket().setSoTimeout(1000 * 60); // one minute
			InetAddress inetaddr = channel.socket().getInetAddress();
			InetSocketAddress addr = new InetSocketAddress(inetaddr.getHostAddress(), channel.socket().getPort());
			Peer peer = new Peer(addr, channel);
			Manager.registerPeer(addr, peer);
		} catch (IOException e) {
			try {
				// cleanup
				channel.close();
			} catch (IOException e2) {

			}
		}
	}

	/**
	 * Initiate a remote connection to peer's listening port.
	 * 
	 * @param peer
	 */
	public void connect(Peer peer) {
		if (!isRunning()) {
			throw new IllegalStateException("Peer acceptor not running");
		}
		executor.submit(new PeerConnectionTask(this, peer));
	}

}
