package fr.enseirb.pirtoupire.centralized;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;

import fr.enseirb.pirtoupire.Configuration;
import fr.enseirb.pirtoupire.TorrentFile;
import fr.enseirb.pirtoupire.util.BufferMap;

public abstract class PeerMessage extends AbstractMessage {
	private final Type type;
	private final ByteBuffer data;
	public static final int PREFIX_SIZE = 4;

	public enum Type {
		INTERESTED, GETPIECES, HAVE, DATA;
	}

	private PeerMessage(Type type, ByteBuffer data) {
		this.type = type;
		this.data = data;
	}

	private PeerMessage(Type type) {
		this.type = type;
		this.data = null;
	}

	public Type getType() {
		return type;
	}

	public ByteBuffer getData() {
		byte[] msg = data.array();
		ByteBuffer buffer = ByteBuffer.allocate(msg.length + 1);
		// add the final \n
		buffer.put(msg).put((byte) '\n').rewind();
		return buffer;
	}

	@Override
	public String toString() {
		return "<PeerMessage " + getType().name() + ": " + getStringRepr() + ">";
	}

	@Override
	public abstract String getStringRepr();

	/**
	 * Parses a raw message from the peer.
	 */
	public static PeerMessage parse(PushbackInputStream in, Peer peer) throws IOException, ProtocolException {
		byte[] bprefix = new byte[PREFIX_SIZE];
		int read = in.read(bprefix);
		String prefix = new String(bprefix);
		System.out.println("[PEER] >> (" + read + " read) " + prefix);
		if (read != PREFIX_SIZE) {
			throw new EOFException("Unable to recieve enough data (" + PREFIX_SIZE + ") for prefix");
		}
		// push back the prefix
		in.unread(bprefix);
		if (prefix.equals("inte")) {
			return InterestedMessage.parse(in, peer);
		} else if (prefix.equals("have")) {
			return HaveMessage.parse(in, peer);
		} else if (prefix.equals("getp")) {
			return GetpiecesMessage.parse(in, peer);
		} else if (prefix.equals("data")) {
			return DataMessage.parse(in, peer);
		} else {
			throw new ProtocolException("Received unknown message (prefix: `" + prefix + "`)");
		}
	}

	/** The messages themselves. **/

	public static class InterestedMessage extends PeerMessage {
		private String key;

		public String getKey() {
			return key;
		}

		private InterestedMessage() {
			super(Type.INTERESTED);
		}

		private InterestedMessage(ByteBuffer data, String key) {
			super(Type.INTERESTED, data);
			this.key = key;
		}

		public static InterestedMessage parse(PushbackInputStream in, Peer peer) throws IOException, ProtocolException {
			in.skip(11); // "interested "
			StringBuilder skey = new StringBuilder();
			byte e = 0x0;
			for (int i = 0; i < Configuration.PROTOCOL_MAX_LENGTH; i++) {
				e = (byte) in.read();
				if (e == '\n' || e == ' ') {
					return new InterestedMessage(null, skey.toString());
				}
				skey.append((char) e);
			}
			throw new ProtocolException("Unable to meet end of line (key too long?)");
		}

		public static InterestedMessage craft(TorrentFile tf) {
			String data = "interested " + tf.getKey();
			return new InterestedMessage(ByteBuffer.wrap(data.getBytes()), tf.getKey());
		}

		@Override
		public String getStringRepr() {
			return "Key: " + getKey();
		}
	}

	public static class HaveMessage extends PeerMessage {
		private final String key;
		private final BufferMap bufferMap;

		public String getKey() {
			return key;
		}

		public BufferMap getBufferMap() {
			return bufferMap;
		}

		private HaveMessage(ByteBuffer data, String key, BufferMap bufferMap) {
			super(Type.HAVE, data);
			this.key = key;
			this.bufferMap = bufferMap;
		}

		public static HaveMessage parse(PushbackInputStream in, Peer peer) throws IOException, ProtocolException {
			in.skip(5); // "have "
			StringBuilder skey = new StringBuilder();
			byte e = 0x0;
			boolean ok = false;
			for (int i = 5; i < Configuration.PROTOCOL_MAX_LENGTH; i++) {
				e = (byte) in.read();
				if (e == ' ') {
					ok = true;
					break;
				}
				skey.append((char) e);
			}
			if (!ok) {
				throw new ProtocolException("Unable to meet end of key (key too long?)");
			}
			String key = skey.toString();
			TorrentFile tf = Manager.torrentFileFromKey(key);
			if (tf == null) {
				throw new ProtocolException("No such torrent: " + key);
			}
			int bmlength = BufferMap.byteLengthOf(tf.getPieceCount());
			byte[] bmap = new byte[bmlength];
			in.read(bmap);
			if (in.read() != '\n') {
				throw new ProtocolException("HAVE message did not end with \\n");
			}
			BufferMap bufferMap = BufferMap.valueOf(bmap, tf.getPieceCount());
			if (bufferMap.capacity() != tf.getPieceCount()) {
				throw new ProtocolException("received buffer map is not same length as known torrent");
			}
			return new HaveMessage(null, key, bufferMap);
		}

		public static HaveMessage craft(String key, BufferMap bufferMap) {
			StringBuilder sb = new StringBuilder("have ");
			sb.append(key).append(" ");
			ByteArrayOutputStream ba = new ByteArrayOutputStream();
			try {
				byte[] bmap = bufferMap.toByteArray();
				ba.write(sb.toString().getBytes());
				ba.write(bmap);
			} catch (IOException e) {
				throw new RuntimeException("Cannot craft HAVE message: " + e.getMessage());
			}
			return new HaveMessage(ByteBuffer.wrap(ba.toByteArray()), key, bufferMap);
		}

		@Override
		public String getStringRepr() {
			return "Key: " + getKey() + " BM: " + getBufferMap();
		}
	}

	public static class GetpiecesMessage extends PeerMessage {
		private final String key;
		private final Set<Integer> pieces;

		public String getKey() {
			return key;
		}

		public Set<Integer> getPieces() {
			return pieces;
		}

		private GetpiecesMessage(ByteBuffer data, String key, Set<Integer> pieces) {
			super(Type.GETPIECES, data);
			this.key = key;
			this.pieces = pieces;
		}

		public static GetpiecesMessage parse(PushbackInputStream in, Peer peer) throws IOException, ProtocolException {
			in.skip(10); // "getpieces "
			StringBuilder skey = new StringBuilder();
			String key = null;
			byte e = 0x0;
			for (int i = 0; i < Configuration.PROTOCOL_MAX_LENGTH; i++) {
				e = (byte) in.read();
				if (e == ' ') {
					key = skey.toString();
					break;
				}
				skey.append((char) e);
			}
			if (key == null) {
				throw new ProtocolException("Unable to meet end of key (key too long?)");
			}
			if (in.read() != '[') {
				throw new ProtocolException("Malformed GETPIECE message (no open bracket)");
			}
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < Configuration.PROTOCOL_MAX_LENGTH; i++) {
				e = (byte) in.read();
				if (e == ']') {
					break;
				}
				sb.append((char) e);
			}
			if (in.read() != '\n') {
				throw new ProtocolException("GETPIECES message did not end with \\n");
			}
			Set<Integer> pieces = new HashSet<Integer>();
			Scanner s = new Scanner(sb.toString());
			s.useDelimiter(Configuration.PROTOCOL_RESERVED_PATTERN);
			while (s.hasNextInt()) {
				pieces.add(s.nextInt());
			}
			try {
				if (s.hasNext()) {
					throw new ProtocolException("Malformed GETPIECES message (not numerics)");
				}
			} finally {
				s.close();
			}
			return new GetpiecesMessage(null, key, pieces);
		}

		public static GetpiecesMessage craft(String key, Set<Integer> pieces) {
			StringBuilder sb = new StringBuilder("getpieces ").append(key).append(" [");
			int i = 0;
			int len = pieces.size();
			for (int p : pieces) {
				sb.append(p);
				if (i++ < len - 1) {
					sb.append(' ');
				}
			}
			sb.append("]");
			return new GetpiecesMessage(ByteBuffer.wrap(sb.toString().getBytes()), key, pieces);
		}

		@Override
		public String getStringRepr() {
			return "Key: " + getKey() + ", pieces: " + getPieces().size();
		}
	}

	public static class DataMessage extends PeerMessage {
		private final String key;
		private final Map<Integer, byte[]> pieces;

		public String getKey() {
			return key;
		}

		public Map<Integer, byte[]> getPieces() {
			return pieces;
		}

		private DataMessage(ByteBuffer data, String key, Map<Integer, byte[]> pieces) {
			super(Type.DATA, data);
			this.key = key;
			this.pieces = pieces;
		}

		public static DataMessage parse(PushbackInputStream in, Peer peer) throws IOException, ProtocolException {
			try {
				in.skip(5); // "data "
				StringBuilder sbkey = new StringBuilder();
				byte e = 0x0;
				for (int i = 0; i < Configuration.PROTOCOL_MAX_LENGTH; i++) {
					e = (byte) in.read();
					if (e == ' ' || e == '[') {
						break;
					}
					sbkey.append((char) e);
				}
				String key = sbkey.toString();
				TorrentFile tf = Manager.torrentFileFromKey(key);
				if (tf == null) {
					throw new ProtocolException("No such torrent: " + key);
				}
				for (int i = 0; i < Configuration.PROTOCOL_MAX_LENGTH; i++) {
					e = (byte) in.read();
					if (e == '[') {
						break;
					}
				}
				Map<Integer, byte[]> pieces = new HashMap<Integer, byte[]>();
				StringBuilder sb = new StringBuilder();
				int index;
				boolean hasIndex = false;
				for (int i = 0; i < Configuration.PROTOCOL_MAX_LENGTH; i++) {
					if (hasIndex) {
						index = Integer.valueOf(sb.toString());
						byte[] blob = new byte[tf.getPieceLength(index)];
						in.read(blob, 0, blob.length);
						pieces.put(index, blob);
						sb = new StringBuilder();
						hasIndex = false;
					} else {
						e = (byte) in.read();
						if (e == ']') {
							break;
						}
						if (e == ' ') {
							continue;
						}
						if (e == ':') {
							hasIndex = true;
							continue;
						}
						if (!Character.isDigit((char) e)) {
							throw new ProtocolException("Malformed DATA (piece index not numeric)");
						}
						sb.append((char) e);
					}
				}
				if (in.read() != '\n') {
					throw new ProtocolException("DATA message did not end with \\n");
				}
				return new DataMessage(null, key, pieces);
			} catch (NumberFormatException e) {
				throw new ProtocolException("Malformed DATA (number parsing)");
			} catch (ArrayIndexOutOfBoundsException e) {
				throw new ProtocolException("Malformed DATA (invalid piece)");
			} catch (BufferUnderflowException e) {
				throw new ProtocolException("Malformed DATA (underflow)");
			} catch (NoSuchElementException e) {
				throw new ProtocolException("Malformed DATA (syntax)");
			}
		}

		public static DataMessage craft(String key, Map<Integer, byte[]> pieces) {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				out.write(("data " + key + " [").getBytes());
				int i = 0;
				int len = pieces.size();
				for (Map.Entry<Integer, byte[]> e : pieces.entrySet()) {
					out.write(String.valueOf(e.getKey()).getBytes("ASCII"));
					out.write(':');
					out.write(e.getValue());
					if (i++ < len - 1) {
						out.write(' ');
					}
				}
				out.write(']');
				return new DataMessage(ByteBuffer.wrap(out.toByteArray()), key, pieces);
			} catch (IOException e) {
				throw new RuntimeException("IOException while crafting DATA message");
			}
		}

		@Override
		public String getStringRepr() {
			return "Key: " + getKey() + " (" + getPieces().size() + " pieces)";
		}
	}
}
