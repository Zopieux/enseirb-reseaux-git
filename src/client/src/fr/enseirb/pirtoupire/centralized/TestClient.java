package fr.enseirb.pirtoupire.centralized;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Set;

import fr.enseirb.pirtoupire.Configuration;
import fr.enseirb.pirtoupire.TorrentFile;
import fr.enseirb.pirtoupire.io.FilesystemTorrent;
import fr.enseirb.pirtoupire.io.TorrentMetadata;

public class TestClient {
	public static void main(String[] args) throws IOException {
		// load the configuration first
		Configuration.loadConfiguration(new File(args[0]));

		String command = args[1];
		if (command.equals("create")) {
			createTorrentFromDisk(new File(args[2]));
		} else if (command.equals("open")) {
			readTorrentMetadata(new File(args[2]));
		} else if (command.equals("tracker")) {
			TrackerConnection tc = trackerConnect("eirb.zopi.eu", 6000, 2222);
			if (args[2].equals("lookup")) {
				trackerLookup(tc, args[3]);
			} else if (args[2].equals("getfile")) {
				trackerGetFile(tc, args[3]);
			}
			tc.close();
		} else if (command.equals("listen")) {
			startPeerAcceptor();
		} else if (command.equals("start")) {
			startClient(new InetSocketAddress(args[2], Integer.valueOf(args[3])));
		}
	}

	private static void createTorrentFromDisk(File path) throws IOException {
		TorrentFile tf = new TorrentFile(path);
		FilesystemTorrent tfs = new FilesystemTorrent(tf, path, true);
		System.out.println(tf);
		System.out.println(tf.toTrackerFormat());
		System.out.println(tfs);
		System.out.println(tfs.getMetadata().bufferMapAsString());

	}

	private static void readTorrentMetadata(File path) throws IOException {
		TorrentMetadata meta = new TorrentMetadata(path);
		FilesystemTorrent tfs = meta.getFilesystemTorrent();
		TorrentFile tf = tfs.getTorrentFile();
		System.out.println(tf);
		System.out.println(tf.toTrackerFormat());
		System.out.println(tfs);
		System.out.println(tfs.getMetadata().bufferMapAsString());
	}

	private static TrackerConnection trackerConnect(String host, int port, int listenPort) throws IOException {
		TrackerConnection tc = new TrackerConnection(new InetSocketAddress(host, port));
		tc.announce(listenPort);
		return tc;
	}

	private static void trackerLookup(TrackerConnection tc, String criterion)
			throws IOException {
		Set<TorrentFile> results = tc.look(criterion);
		for (TorrentFile tf : results) {
			System.out.println(tf);
		}
	}

	private static void trackerGetFile(TrackerConnection tc, String key)
			throws IOException {
		TorrentFile tf = new TorrentFile("foo", key, 32l, 32);
		Set<InetSocketAddress> results = tc.getPeersForTorrentFile(tf);
		System.out.println("RESULTS");
		for (InetSocketAddress addr : results) {
			System.out.println(addr);
		}
	}

	private static void startPeerAcceptor() throws IOException {
		Manager.setTrackerAddress(new InetSocketAddress("eirb.zopi.eu", 6000));

		// create seeded file
		File f = new File("jazzhorse.wav");
		assert f.exists();
		TorrentFile tf = new TorrentFile(f, 2048);
		FilesystemTorrent tfs = Manager.seedNewTorrent(tf, f);
		System.out.println(tf);
		System.out.println("Seeding: " + tfs.getMetadata().bufferMapAsString());

		PeerAcceptor pa = new PeerAcceptor(Configuration.PEERLISTENER_ADDR);
		pa.start();
		System.out.println("Started acceptor");

		// tracker connection
		System.out.println(Configuration.toStaticString());
	}

	private static void startClient(InetSocketAddress addr) throws IOException {
		System.setProperty("java.net.preferIPv4Stack", "true");

		// start peer acceptor
		Manager.startPeerAcceptor();
		// start tracker
		Manager.setTrackerAddress(addr);
		Manager.startTrackerConnection();
		// start peers update thread
		Manager.startUpdate();
		Manager.startUpdateNewPeers();

		// let's leech a file
		final String fn = "jazzhorse.wav";
		Set<TorrentFile> results = Manager.trackerLook("psize > 400 and name=" + fn);
		TorrentFile leechedtf = results.iterator().next();
		System.out.println("[MAIN] Found " + fn + ": " + leechedtf);
		FilesystemTorrent fst = Manager.leechNewTorrent(leechedtf, new File("/tmp/" + fn));
		System.out.println("[MAIN] " + fst.getMetadata().bufferMapAsString());
	}
}
