package fr.enseirb.pirtoupire.centralized;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Set;
import java.util.logging.Logger;

import fr.enseirb.pirtoupire.Configuration;
import fr.enseirb.pirtoupire.TorrentFile;
import fr.enseirb.pirtoupire.io.FilesystemTorrent;
import fr.enseirb.pirtoupire.util.BufferMap;
import fr.enseirb.pirtoupire.util.Utils;

/**
 * Represents a torrent being seeded and/or leeched by peers. Backed by an underlying {@link FilesystemTorrent}.
 */
public class Torrent extends Observable implements Comparable<Torrent> {
	private final FilesystemTorrent fst;
	private final Map<Peer, Set<Integer>> requestedPieces;
	private final ArrayList<Set<Peer>> availablePieces;
	private boolean seeding;
	private boolean stop;
	private final Object lockObj = new Object();
	private final Thread updateThread;

	private static final Logger logger = Logger.getLogger(Torrent.class.getName());

	public Torrent(FilesystemTorrent fst, boolean seeding) {
		this.stop = false;
		this.seeding = seeding;
		this.fst = fst;
		availablePieces = new ArrayList<Set<Peer>>();
		requestedPieces = new HashMap<Peer, Set<Integer>>();
		for (int p = 0; p < fst.getTorrentFile().getPieceCount(); p++) {
			// initialize empty peer set for each piece
			availablePieces.add(new HashSet<Peer>());
		}
		this.updateThread = new Thread(new PieceUpdater(this));
		this.updateThread.start();
		logger.fine("New Torrent from " + fst);
	}

	public FilesystemTorrent getFilesystemTorrent() {
		return fst;
	}

	public boolean isSeeding() {
		return !stop && seeding;
	}

	public void setSeeding(boolean seeding) {
		this.seeding = seeding;
		logger.fine("Torrent " + this + " is now seeding");
		setChanged();
		notifyObservers();
	}

	public TorrentFile getFileTorrent() {
		return fst.getTorrentFile();
	}

	public synchronized boolean isLeeching() {
		return !stop && !isCompleted();
	}

	public synchronized boolean isActive() {
		return isSeeding() || isLeeching();
	}

	public synchronized boolean isCompleted() {
		return fst.getMetadata().isCompleted();
	}

	public void stop() {
		stop = true;
		updateThread.interrupt();
		setSeeding(false);
		fst.close();
		logger.fine("Torrent " + this + " stopped");
		setChanged();
		notifyObservers();
	}

	public BufferMap getRequestedBufferMap(Peer peer) {
		int pc = getFileTorrent().getPieceCount();
		BufferMap bm = new BufferMap(pc);
		synchronized (requestedPieces) {
			Set<Integer> pieces = requestedPieces.get(peer);
			if (pieces == null) {
				return bm;
			}
			for (int i : pieces) {
				bm.set(i);
			}
		}
		return bm;
	}

	public synchronized void addRequestedPieces(Set<Integer> rp, Peer peer) {
		logger.fine("Torrent " + this + " requests " + rp.size() + " pieces from peer " + peer);
		if (requestedPieces.containsKey(peer)) {
			requestedPieces.get(peer).addAll(rp);
		} else {
			requestedPieces.put(peer, new HashSet<Integer>(rp));
		}
		setChanged();
		notifyObservers();
	}

	public synchronized int pieceRequestCountForPeer(Peer peer) {
		Set<Integer> s = requestedPieces.get(peer);
		if (s == null) {
			return 0;
		}
		return s.size();
	}

	public synchronized float getCompletedRatio() {
		return (float) fst.getMetadata().completedPieceCount() / fst.getTorrentFile().getPieceCount();
	}

	public synchronized int getSeederCount() {
		return requestedPieces.size();
	}

	public synchronized Set<Peer> getSeeders() {
		return requestedPieces.keySet();
	}

	public synchronized Map<Peer, Set<Integer>> availableInterestingPieces() {
		Map<Peer, Set<Integer>> res = new HashMap<Peer, Set<Integer>>();
		int p = -1;
		for (Set<Peer> peers : availablePieces) {
			p++;
			if (getFilesystemTorrent().getMetadata().isPieceCompleted(p)) {
				continue;
			}
			for (Peer peer : peers) {
				Set<Integer> pieces = requestedPieces.get(peer);
				if (pieces != null && pieces.contains(p)) {
					// do not re-ask same peer!
					continue;
				}
				if (res.containsKey(peer)) {
					res.get(peer).add(p);
				} else {
					Set<Integer> s = new HashSet<Integer>();
					s.add(p);
					res.put(peer, s);
				}
			}
		}
		return res;
	}

	/**
	 * The buffermap we don't have and have not requested neither
	 */
	private synchronized BufferMap interestingBufferMap(BufferMap bm, Peer peer) {
		bm.andNot(fst.getMetadata().getCompletedBufferMap());
		bm.andNot(getRequestedBufferMap(peer));
		return bm;
	}

	@Override
	public String toString() {
		return "<Torrent " + getFileTorrent().toTrackerFormat() + " (seeding: " + isSeeding() + ", leeching: "
				+ isLeeching() + ")>";
	}

	@Override
	public int compareTo(Torrent o) {
		return fst.getTorrentFile().compareTo(o.getFilesystemTorrent().getTorrentFile());
	}

	/** Event handlers **/

	/**
	 * Handles the "have" response, after a successful "interested" or "update" message.
	 * 
	 * @param peer
	 * @param peerBufferMap
	 */
	public synchronized void eventPeerHave(Peer peer, BufferMap peerBufferMap) {
		int nbPieces = fst.getTorrentFile().getPieceCount();
		if (peerBufferMap.capacity() != nbPieces) {
			logger.warning("Torrent " + this + ": buffermap of peer " + peer + " has an invalid size");
			return;
		}
		for (int i = 0; i < nbPieces; i++) {
			if (peerBufferMap.get(i)) {
				availablePieces.get(i).add(peer);
			} else {
				availablePieces.get(i).remove(peer);
			}
		}
		setChanged();
		notifyObservers();
		BufferMap interesting = interestingBufferMap(peerBufferMap, peer);
		if (!interesting.isEmpty()) {
			// wakeUpdate();
			logger.info("Torrent " + this + ": peer " + peer + " has " + peerBufferMap.cardinality()
					+ " interesting pieces");
		}
	}

	/**
	 * Handles the "data" response, after a successful "getpieces" messages.
	 * 
	 * @param peer
	 * @param pieces
	 */
	public void eventPeerPieceData(Peer peer, Map<Integer, byte[]> pieces) {
		synchronized (requestedPieces) {
			if (!requestedPieces.containsKey(peer)) {
				return;
			}
		}
		for (Map.Entry<Integer, byte[]> piece : pieces.entrySet()) {
			int index = piece.getKey();
			// remove it from requested
			synchronized (requestedPieces) {
				boolean was_in = requestedPieces.get(peer).remove(index);
				if (!was_in) {
					logger.warning("Torrent " + this + ": peer " + peer + " sent the not-requested piece " + index);
					continue;
				}
			}
			// write the piece!
			try {
				fst.write(index, piece.getValue());
				setChanged();
				notifyObservers();
			} catch (IOException e) {
				throw new RuntimeException("Unable to write piece " + piece.getKey() + " for "
						+ fst.getTorrentFile(),
						e);
			}
		}
		wakeUpdate();
	}

	public PeerMessage.HaveMessage eventPeerInterested(Peer peer) {
		String key = fst.getTorrentFile().getKey();
		synchronized (fst) {
			if (isSeeding()) {
				// available and seedable
				logger.fine("Torrent " + this + ": sending my buffermap to " + peer);
				return PeerMessage.HaveMessage
						.craft(key, getFilesystemTorrent().getMetadata().getCompletedBufferMap());
			} else {
				// not seedable
				logger.fine("Torrent " + this + ": sending empty buffermap because I'm not seeding this");
				return PeerMessage.HaveMessage.craft(key, new BufferMap(fst
						.getTorrentFile().getPieceCount()));
			}
		}
	}

	public void eventPeerUpdate(Peer peer, BufferMap bm) {
		logger.fine("Torrent " + this + ": peer " + peer + " sent his updated buffermap");
		synchronized (availablePieces) {
			for (int p = 0; p < getFileTorrent().getPieceCount(); p++) {
				if (bm.get(p)) {
					availablePieces.get(p).add(peer);
				} else {
					availablePieces.get(p).remove(peer);
				}
			}
		}
		wakeUpdate();
	}

	/**
	 * Handles peer disconnection. We have to remove available pieces from this peer.
	 * 
	 * @param peer
	 */
	public synchronized void eventPeerDisconnected(Peer peer) {
		logger.info("Torrent " + this + ": peer " + peer + " disconnected");
		for (int i = 0; i < availablePieces.size(); i++) {
			availablePieces.get(i).remove(peer);
		}
		if (requestedPieces.containsKey(peer)) {
			requestedPieces.get(peer).clear();
			requestedPieces.remove(peer);
		}
		setChanged();
		notifyObservers();
		wakeUpdate();
	}

	public String pieceAvailabilityMap() {
		StringBuilder sb = new StringBuilder("[");
		synchronized (getFilesystemTorrent()) {
			for (int p = 0; p < getFileTorrent().getPieceCount(); p++) {
				if (getFilesystemTorrent().getMetadata().isPieceCompleted(p)) {
					sb.append("#");
				} else {
					sb.append(Utils.HEX_ARRAY[Math.min(16, availablePieces.get(p).size())]);
				}
			}
		}
		sb.append(']');
		return sb.toString();
	}

	public void wakeUpdate() {
		synchronized (lockObj) {
			lockObj.notifyAll();
		}
	}

	private class PieceUpdater implements Runnable {
		private final Torrent torrent;

		public PieceUpdater(Torrent torrent) {
			this.torrent = torrent;
		}

		private boolean update() {
			if (torrent.isCompleted()) {
				return true;
			}
			Set<Integer> all_requested = new HashSet<Integer>();
			// peer → (avail pieces of peer)
			for (Entry<Peer, Set<Integer>> e : torrent.availableInterestingPieces().entrySet()) {
				logger.info("Torrent " + torrent + ": peer " + e.getKey() + " has "
						+ e.getValue().size()
						+ " intersting pieces");

				Peer peer = e.getKey();
				Set<Integer> avail = e.getValue();
				// get available pieces
				avail.removeAll(all_requested);
				if (avail.size() == 0) {
					// no need to continue
					System.out.println("NOPE NO AVAIL");
					continue;
				}
				// the max amount of pieces to be requested
				int take_amount = Math.min(
						Configuration.PROTOCOL_MAX_PEER_PIECES
								- torrent.pieceRequestCountForPeer(peer),
						torrent.getFileTorrent().getMaxNumberRequestablePieces()
						);
				if (take_amount == 0) {
					System.out.println("NOPE NO AMOUNT");
					continue;
				}
				Set<Integer> requests = new HashSet<Integer>();
				Iterator<Integer> iter = avail.iterator();
				while (iter.hasNext() && requests.size() < take_amount) {
					requests.add(iter.next());
				}
				System.out.println("**** REQUESTING **** " + requests.size() + " new pieces");
				all_requested.addAll(requests);
				peer.requestPieces(torrent, requests);
			}
			return false;
		}

		@Override
		public void run() {
			System.out.println("========= ENTERING UPDATE FOR " + torrent);
			while (torrent.isActive()) {
				try {
					synchronized (lockObj) {
						System.out.println("========= **WAITING** UPDATE FOR " + torrent);
						lockObj.wait(1000);
					}
					System.out.println("========= DOING UPDATE FOR " + torrent);
					if (update()) {
						System.out.println("========= STOP UPDATE FOR " + torrent);
						return;
					}
					// Thread.sleep(10); // give it some rest
				} catch (InterruptedException e) {
					return;
				}
			}
		}
	}
}
