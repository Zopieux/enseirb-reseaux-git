package fr.enseirb.pirtoupire.centralized;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import fr.enseirb.pirtoupire.Configuration;
import fr.enseirb.pirtoupire.TorrentFile;
import fr.enseirb.pirtoupire.centralized.AbstractMessage.ProtocolException;

public class TrackerConnection {
	private final Socket socket;
	private final BufferedReader readStream;
	private final PrintWriter writeStream;
	private boolean announced = false;
	private Timer updateTimer;

	public TrackerConnection(InetSocketAddress addr) throws IOException {
		socket = new Socket(addr.getAddress(), addr.getPort());
		writeStream = new PrintWriter(socket.getOutputStream());
		readStream = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
	}

	private void debug(String msg, boolean out) {
		System.out.println("[TRACKER] " + (out ? "<<" : ">>") + " " + msg);
	}

	public synchronized void announce(int listenPort) {
		announce(listenPort, new HashSet<TorrentFile>(), new HashSet<TorrentFile>());
	}

	public synchronized void announce(int listenPort, Set<TorrentFile> seeds, Set<TorrentFile> leechs) {
		if (announced) {
			throw new RuntimeException("Already announced");
		}
		TrackerMessage.AnnounceMessage request = TrackerMessage.AnnounceMessage.craft(listenPort, seeds, leechs);
		debug(request.getData(), true);
		writeStream.write(request.getData());
		writeStream.flush();
		try {
			String in = readStream.readLine();
			debug(in, false);
			TrackerMessage.AnnounceMessageResponse.parse(in, request);
		} catch (IOException e) {
			throw new RuntimeException("io error when parsing `announce` response", e);
		} catch (ProtocolException e) {
			throw new RuntimeException("protocol error when parsing `announce` response", e);
		}
		announced = true;
	}

	public synchronized Set<TorrentFile> look(String criterions) {
		ensureAnnounced();
		TrackerMessage.LookMessage request = TrackerMessage.LookMessage.craft(criterions);
		debug(request.getData(), true);
		writeStream.write(request.getData());
		writeStream.flush();
		try {
			String in = readStream.readLine();
			debug(in, false);
			return TrackerMessage.LookMessageResponse.parse(in, request).getResults();
		} catch (IOException e) {
			throw new RuntimeException("io error when parsing `look` response", e);
		} catch (ProtocolException e) {
			throw new RuntimeException("protocol error when parsing `look` response", e);
		}
	}

	public Set<InetSocketAddress> getPeersForTorrentFile(TorrentFile torrent) {
		ensureAnnounced();
		TrackerMessage.GetfileMessage request = TrackerMessage.GetfileMessage.craft(torrent);
		debug(request.getData(), true);
		writeStream.write(request.getData());
		writeStream.flush();
		try {
			String in = readStream.readLine();
			debug(in, false);
			return TrackerMessage.GetfileMessageResponse.parse(in, request).getPeers();
		} catch (IOException e) {
			throw new RuntimeException("io error when parsing `look` response", e);
		} catch (ProtocolException e) {
			throw new RuntimeException("protocol error when parsing `look` response", e);
		}
	}

	public void update(Set<TorrentFile> seeds, Set<TorrentFile> leechs) {
		ensureAnnounced();
		TrackerMessage.UpdateMessage request = TrackerMessage.UpdateMessage.craft(seeds, leechs);
		debug(request.getData(), true);
		writeStream.write(request.getData());
		writeStream.flush();
		try {
			String in = readStream.readLine();
			debug(in, false);
			TrackerMessage.UpdateMessageResponse.parse(in, request);
		} catch (IOException e) {
			throw new RuntimeException("io error when parsing `look` response", e);
		} catch (ProtocolException e) {
			throw new RuntimeException("protocol error when parsing `look` response", e);
		}
	}

	public void close() {
		try {
			updateTimer.cancel();
			socket.shutdownInput();
			socket.shutdownOutput();
			socket.close();
			readStream.close();
			writeStream.close();
			System.out.println("[TRACKER] closed");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isAlive() {
		return !socket.isClosed() && socket.isConnected();
	}

	public boolean isAnnounced() {
		return announced;
	}

	private synchronized void ensureAnnounced() {
		if (!announced) {
			throw new RuntimeException("Not announced");
		}
	}

	public void startUpdater() {
		ensureAnnounced();
		updateTimer = new Timer();
		updateTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				if (!isAnnounced()) {
					return;
				}
				update(Manager.getSeededTorrents(), Manager.getLeechedTorrents());
				System.out.println("[TRACKER] Updated");
			}
		}, Configuration.PROTOCOL_UPDATE_TRACKER * 1000l, Configuration.PROTOCOL_UPDATE_TRACKER * 1000l);
	}
}
