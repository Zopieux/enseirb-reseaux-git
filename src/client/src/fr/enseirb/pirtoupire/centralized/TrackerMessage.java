package fr.enseirb.pirtoupire.centralized;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;

import fr.enseirb.pirtoupire.Configuration;
import fr.enseirb.pirtoupire.TorrentFile;

public abstract class TrackerMessage extends AbstractMessage {
	private final Type type;
	private final String data;

	public enum Type {
		ANNOUNCE, LOOK, GETFILE, UPDATE;
	}

	private TrackerMessage(Type type) {
		this.type = type;
		this.data = null;
	}

	private TrackerMessage(Type type, String data) {
		this.type = type;
		this.data = data;
	}

	public Type getType() {
		return type;
	}

	public String getData() {
		// add the final \n
		return data + "\n";
	}

	@Override
	public String toString() {
		return "<TrackerMessage " + getType().name() + " " + getStringRepr() + ">";
	}

	/**
	 * Parses a raw response from the tracker, after sending the `request` message
	 */
	public static TrackerMessage parse(ByteBuffer data, TrackerMessage request) throws IllegalStateException {
		String msg = new String(data.array());
		if (request instanceof AnnounceMessage && msg.startsWith("ok")) {
			return AnnounceMessageResponse.parse(data, request);
		} else if (request instanceof LookMessage && msg.startsWith("list ")) {
			return LookMessageResponse.parse(data, request);
		} else if (request instanceof UpdateMessage && msg.startsWith("update ")) {
			return UpdateMessageResponse.parse(data, request);
		} else if (request instanceof GetfileMessage && msg.startsWith("peers ")) {
			return GetfileMessageResponse.parse(data, request);
		} else {
			String starting = msg.substring(0, Math.min(msg.length() - 1, 16));
			throw new ProtocolException("Received unknown message from tracker (starting with `" + starting + "`");
		}
	}

	public static class AnnounceMessage extends TrackerMessage {
		private final int port;
		private final Set<TorrentFile> seeds;
		private final Set<TorrentFile> leechs;

		public int getPort() {
			return port;
		}

		public Set<TorrentFile> getSeeds() {
			return seeds;
		}

		public Set<TorrentFile> getLeechs() {
			return leechs;
		}

		public AnnounceMessage(String data, int port, Set<TorrentFile> seeds, Set<TorrentFile> leechs) {
			super(Type.ANNOUNCE, data);
			this.port = port;
			this.seeds = seeds;
			this.leechs = leechs;
		}

		public static AnnounceMessage craft(int port, Set<TorrentFile> seeds, Set<TorrentFile> leechs) {
			StringBuilder data = new StringBuilder("announce listen ");
			data.append(port).append(" seed [");
			Iterator<TorrentFile> it = seeds.iterator();
			while (it.hasNext()) {
				data.append(it.next().toTrackerFormat());
				if (it.hasNext()) {
					data.append(" ");
				}
			}
			data.append("] leech [");
			it = leechs.iterator();
			while (it.hasNext()) {
				data.append(it.next().getKey());
				if (it.hasNext()) {
					data.append(" ");
				}
			}
			data.append("]");
			return new AnnounceMessage(data.toString(), port, seeds, leechs);
		}

		@Override
		public String getStringRepr() {
			return "port: " + getPort() + ", " + getSeeds().size() + " seeds, " + getLeechs().size() + " leechs";
		}
	}

	public static class AnnounceMessageResponse extends TrackerMessage {
		public AnnounceMessageResponse() {
			super(Type.ANNOUNCE);
		}

		public static AnnounceMessageResponse parse(String data, AnnounceMessage request) throws ProtocolException {
			if (data.equals("ok")) {
				return new AnnounceMessageResponse();
			} else {
				throw new ProtocolException("Invalid response from tracker");
			}
		}

		@Override
		public String getStringRepr() {
			return "ok";
		}
	}

	public static class LookMessage extends TrackerMessage {
		private final String criterions;

		public String getCriterions() {
			return criterions;
		}

		public LookMessage(String data, String criterions) {
			super(Type.LOOK, data);
			this.criterions = criterions;
		}

		public static LookMessage craft(String criterions) {
			String data = "look [" + criterions + "]";
			return new LookMessage(data, criterions);
		}

		@Override
		public String getStringRepr() {
			return "Criterions: " + getCriterions();
		}
	}

	public static class LookMessageResponse extends TrackerMessage {
		private final Set<TorrentFile> results;

		public LookMessageResponse(String data, Set<TorrentFile> results) {
			super(Type.LOOK);
			this.results = results;
		}

		public Set<TorrentFile> getResults() {
			return results;
		}

		public static final LookMessageResponse parse(String data, LookMessage request) throws ProtocolException {
			String name, key;
			long length;
			int piece_length;
			Scanner s = new Scanner(data);
			s.useDelimiter(Configuration.PROTOCOL_RESERVED_PATTERN);
			Set<TorrentFile> results = new HashSet<TorrentFile>();
			try {
				s.skip("list\\s+\\[");
				while (s.hasNext() && !s.hasNext("\\]")) {
					name = s.next();
					length = s.nextLong();
					piece_length = s.nextInt();
					key = s.next();
					TorrentFile tf = new TorrentFile(name, key, length,
							piece_length);
					results.add(tf);
				}
				return new LookMessageResponse(data, results);
			} catch (InputMismatchException e) {
				throw new ProtocolException("Malformed response from tracker for `look` (format)");
			} catch (NoSuchElementException e) {
				throw new ProtocolException("Malformed response from tracker for `look` (syntax)");
			} finally {
				s.close();
			}
		}

		@Override
		public String getStringRepr() {
			return "List of " + getResults().size() + " results";
		}
	}

	public static class GetfileMessage extends TrackerMessage {
		private final TorrentFile torrentFile;

		public TorrentFile getTorrentFile() {
			return torrentFile;
		}

		public GetfileMessage(String data, TorrentFile torrentFile) {
			super(Type.GETFILE, data);
			this.torrentFile = torrentFile;
		}

		public static GetfileMessage craft(TorrentFile torrentFile) {
			String data = "getfile " + torrentFile.getKey();
			return new GetfileMessage(data, torrentFile);
		}

		@Override
		public String getStringRepr() {
			return "Key: " + getTorrentFile().getKey();
		}
	}

	public static class GetfileMessageResponse extends TrackerMessage {
		private final Set<InetSocketAddress> peers;

		public Set<InetSocketAddress> getPeers() {
			return peers;
		}

		public GetfileMessageResponse(String data, Set<InetSocketAddress> peers) {
			super(Type.GETFILE, data);
			this.peers = peers;

		}

		public static GetfileMessageResponse parse(String data, GetfileMessage request) throws ProtocolException {
			Scanner s = new Scanner(data);
			s.useDelimiter(Configuration.PROTOCOL_RESERVED_PATTERN);
			Set<InetSocketAddress> results = new HashSet<InetSocketAddress>();
			try {
				s.skip("peers\\s+");
				s.skip(request.getTorrentFile().getKey());
				s.skip("\\s+\\[");
				while (s.hasNext()) {
					s.next("([\\d\\.]+):(\\d+)");
					InetSocketAddress addr = new InetSocketAddress(
							s.match().group(1), Integer.valueOf(s.match().group(2)));
					results.add(addr);
				}
				return new GetfileMessageResponse(data, results);
			} catch (NumberFormatException e) {
				throw new ProtocolException("Malformed response from tracker for `getfile` (wrong port value)");
			} catch (IllegalArgumentException e) {
				throw new ProtocolException("Malformed response from tracker for `getfile` (wrong host/port)");
			} catch (NoSuchElementException e) {
				throw new ProtocolException("Malformed response from tracker for `getfile` (syntax)");
			} finally {
				s.close();
			}
		}

		@Override
		public String getStringRepr() {
			return "list of " + getPeers().size() + " peers";
		}
	}

	public static class UpdateMessage extends TrackerMessage {
		private final Set<TorrentFile> seeds;
		private final Set<TorrentFile> leechs;

		public Set<TorrentFile> getSeeds() {
			return seeds;
		}

		public Set<TorrentFile> getLeechs() {
			return leechs;
		}

		public UpdateMessage(String data, Set<TorrentFile> seeds, Set<TorrentFile> leechs) {
			super(Type.GETFILE, data);
			this.seeds = seeds;
			this.leechs = leechs;
		}

		public static UpdateMessage craft(Set<TorrentFile> seeds, Set<TorrentFile> leechs) {
			StringBuilder data = new StringBuilder("update seed [");
			Iterator<TorrentFile> it = seeds.iterator();
			while (it.hasNext()) {
				data.append(it.next().getKey());
				if (it.hasNext()) {
					data.append(" ");
				}
			}
			data.append("] leech [");
			it = leechs.iterator();
			while (it.hasNext()) {
				data.append(it.next().getKey());
				if (it.hasNext()) {
					data.append(" ");
				}
			}
			data.append("]");
			return new UpdateMessage(data.toString(), seeds, leechs);
		}

		@Override
		public String getStringRepr() {
			return getSeeds().size() + " seeds, " + getLeechs().size() + " leechs";
		}
	}

	public static class UpdateMessageResponse extends TrackerMessage {
		public UpdateMessageResponse() {
			super(Type.UPDATE);
		}

		public static UpdateMessageResponse parse(String data, UpdateMessage request) throws ProtocolException {
			if (data.equals("ok")) {
				return new UpdateMessageResponse();
			} else {
				throw new ProtocolException("Invalid response from tracker for `update`");
			}
		}

		@Override
		public String getStringRepr() {
			return "ok";
		}
	}

}
