package fr.enseirb.pirtoupire.io;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import fr.enseirb.pirtoupire.TorrentFile;

public class FilesystemTorrent {

	private RandomAccessFile raf;
	private final TorrentFile torrentFile;
	private final TorrentMetadata metadata;
	private final File destinationFile;

	public FilesystemTorrent(TorrentFile torrentFile, File destinationFile,
			TorrentMetadata metadata) throws IOException {
		this.torrentFile = torrentFile;
		this.destinationFile = destinationFile.getCanonicalFile();
		this.metadata = metadata;
		init();
	}

	public FilesystemTorrent(TorrentFile torrentFile, File destinationFile,
			boolean completed) throws IOException {
		this.torrentFile = torrentFile;
		this.destinationFile = destinationFile.getCanonicalFile();
		this.metadata = new TorrentMetadata(this, completed);
		init();
	}

	public synchronized void init() throws IOException {
		// create the parent directories if needed
		destinationFile.getParentFile().mkdirs();
		raf = new RandomAccessFile(destinationFile, "rw");
		raf.setLength(torrentFile.getLength());
	}

	public synchronized void write(int pieceIndex, byte[] block)
			throws IOException {
		torrentFile.validatePieceIndex(pieceIndex);
		if (metadata.isPieceCompleted(pieceIndex)) {
			System.out.println("WARNING: writing already completed piece "
					+ pieceIndex + " in " + this);
		}
		int length = block.length;
		assert length == torrentFile.getPieceLength(pieceIndex);
		int begin = pieceIndex * torrentFile.getPieceLength();
		System.out.println("Writing piece " + pieceIndex + " at " + begin + " length " + length);
		try {
			raf.seek(begin);
			raf.write(block);
			System.out.println("THE PIECE WAS WROTE AT " + pieceIndex);
		} catch (IndexOutOfBoundsException e) {
			System.out.println("ONOES OUT OF BOUND " + begin + " " + length);
		}
		metadata.setPieceCompleted(pieceIndex);
	}

	public synchronized byte[] read(int pieceIndex) throws IOException {
		torrentFile.validatePieceIndex(pieceIndex);
		assert metadata.isPieceCompleted(pieceIndex);
		int length = torrentFile.getPieceLength(pieceIndex);
		int begin = pieceIndex * torrentFile.getPieceLength();
		byte[] buff = new byte[length];
		raf.seek(begin);
		int read = raf.read(buff);
		assert read == length;
		return buff;
	}

	public synchronized void close() {
		try {
			raf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public File getDestinationFile() {
		return destinationFile;
	}

	public TorrentFile getTorrentFile() {
		return torrentFile;
	}

	public TorrentMetadata getMetadata() {
		return metadata;
	}

	@Override
	public String toString() {
		return "FilesystemTorrent { " + torrentFile.getSignature() + " at "
				+ destinationFile + " }";
	}
}
