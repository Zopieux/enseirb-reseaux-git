package fr.enseirb.pirtoupire.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import fr.enseirb.pirtoupire.Configuration;
import fr.enseirb.pirtoupire.TorrentFile;
import fr.enseirb.pirtoupire.util.BufferMap;
import fr.enseirb.pirtoupire.util.Utils;

public class TorrentMetadata {
	private final FilesystemTorrent tfs;
	private BufferMap bufferMap;

	/**
	 * Constructs a TorrentMetadata from its file on disk.
	 * 
	 * @param path
	 *            the path to the disk file to read
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public TorrentMetadata(File path) throws FileNotFoundException, IOException {
		path = path.getCanonicalFile();

		ObjectInput ois = new ObjectInputStream(new FileInputStream(path));

		File destinationFile = new File(ois.readUTF()).getCanonicalFile();
		try {
			bufferMap = (BufferMap) ois.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String torrentName = ois.readUTF();
		String torrentKey = ois.readUTF();
		long torrentLength = ois.readLong();
		int torrentPieceLength = ois.readInt();

		TorrentFile torrentFile = new TorrentFile(torrentName, torrentKey,
				torrentLength, torrentPieceLength);
		tfs = new FilesystemTorrent(torrentFile, destinationFile, this);

		ois.close();

		// are the signature equals?
		assert getFile().equals(path);
	}

	/**
	 * Constructs a TorrentMeta from the corresponding FilesystemTorrent.
	 * 
	 * @param tfs
	 * @param completed
	 *            whether the file is completed (true) or empty (to be downloaded)
	 */
	public TorrentMetadata(FilesystemTorrent tfs, boolean completed) {
		this.tfs = tfs;
		int pc = tfs.getTorrentFile().getPieceCount();
		bufferMap = new BufferMap(pc);
		if (completed) {
			bufferMap.fill(true);
		}
		write();
	}

	public FilesystemTorrent getFilesystemTorrent() {
		return tfs;
	}

	public synchronized BufferMap getCompletedBufferMap() {
		return bufferMap.clone();
	}

	public synchronized boolean isCompleted() {
		return completedPieceCount() == tfs.getTorrentFile().getPieceCount();
	}

	public synchronized boolean isPieceCompleted(int pieceIndex) {
		tfs.getTorrentFile().validatePieceIndex(pieceIndex);
		return bufferMap.get(pieceIndex);
	}

	public synchronized void setPieceCompleted(int pieceIndex) {
		tfs.getTorrentFile().validatePieceIndex(pieceIndex);
		bufferMap.set(pieceIndex);
		write();
	}

	public synchronized int completedPieceCount() {
		return bufferMap.cardinality();
	}

	public synchronized void write() {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(getFile(), false));
			oos.writeUTF(tfs.getDestinationFile().getPath());
			oos.writeObject(bufferMap);
			oos.writeUTF(tfs.getTorrentFile().getName());
			oos.writeUTF(tfs.getTorrentFile().getKey());
			oos.writeLong(tfs.getTorrentFile().getLength());
			oos.writeInt(tfs.getTorrentFile().getPieceLength());
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private File getFile() throws IOException {
		// file name is "SHA(destinationFile) . SHA(signature) . meta"
		String destHash = Utils.computeStringHash(tfs.getDestinationFile()
				.getPath());
		String fname = destHash + "." + tfs.getTorrentFile().getSignature()
				+ ".meta";
		return new File(Configuration.SESSION_DIRECTORY, fname)
				.getCanonicalFile();
	}

	@Override
	public String toString() {
		return "TorrentMetada { for " + tfs + " }";
	}

	public synchronized String bufferMapAsString() {
		StringBuilder sb = new StringBuilder("[");
		int size = tfs.getTorrentFile().getPieceCount();
		for (int i = 0; i < size; i++) {
			sb.append(bufferMap.get(i) ? "#" : " ");
		}
		sb.append("] (").append(completedPieceCount()).append("/").append(size)
				.append(")");
		return sb.toString();
	}
}
