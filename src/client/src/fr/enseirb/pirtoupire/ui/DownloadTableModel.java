package fr.enseirb.pirtoupire.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import fr.enseirb.pirtoupire.centralized.Torrent;

public final class DownloadTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 4835232752527041478L;
	private static final String[] HEADERS = new String[] { "Nom", "Progrès", "Seeders", };
	private List<Torrent> data;

	public DownloadTableModel() {
		super();
		data = new ArrayList<Torrent>();
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		// read-only except for button
		return column == 3;
	}

	public void setData(Set<Torrent> data) {
		this.data = new ArrayList<Torrent>(data);
		fireTableDataChanged();
	}

	public void dataUpdated() {
		fireTableDataChanged();
	}

	@Override
	public Class<?> getColumnClass(int col) {
		if (col == 1) {
			return Float.class;
		} else if (col == 2 || col == 3) {
			return Integer.class;
		} else if (col == 4) {
			return ButtonColumn.class;
		}
		return String.class;
	}

	@Override
	public Object getValueAt(int row, int col) {
		if (col == 0) {
			// TODO more lines
			return data.get(row).getFilesystemTorrent().getDestinationFile().getPath();
		} else if (col == 1) {
			return data.get(row).getCompletedRatio();
		} else if (col == 2) {
			return data.get(row).getSeederCount();
		}
		return null;
	}

	public Torrent torrentAt(int row) {
		return data.get(row);
	}

	@Override
	public String getColumnName(int col) {
		return HEADERS[col];
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return HEADERS.length;
	}
}
