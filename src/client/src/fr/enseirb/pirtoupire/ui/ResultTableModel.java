package fr.enseirb.pirtoupire.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import fr.enseirb.pirtoupire.TorrentFile;

public final class ResultTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 4835232752527041478L;
	private static final String[] HEADERS = new String[] { "Nom", "Taille", "Clé unique", "Action" };
	private List<TorrentFile> data;

	public ResultTableModel() {
		super();
		data = new ArrayList<TorrentFile>();
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		// read-only except for button
		return column == 3;
	}

	public void setData(Set<TorrentFile> data) {
		this.data = new ArrayList<TorrentFile>(data);
		fireTableDataChanged();
	}

	@Override
	public Class<?> getColumnClass(int col) {
		if (col == 1) {
			return Long.class;
		} else if (col == 3) {
			return ButtonColumn.class;
		}
		return String.class;
	}

	@Override
	public Object getValueAt(int row, int col) {
		if (col == 0) {
			return data.get(row).getName();
		} else if (col == 1) {
			return data.get(row).getLength();
		} else if (col == 2) {
			return data.get(row).getKey();
		} else if (col == 3) {
			return "Télécharger";
		}
		return null;
	}

	public TorrentFile getTorrent(int row) {
		return data.get(row);
	}

	@Override
	public String getColumnName(int col) {
		return HEADERS[col];
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return HEADERS.length;
	}
}
