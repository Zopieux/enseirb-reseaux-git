package fr.enseirb.pirtoupire.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.enseirb.pirtoupire.TorrentFile;
import fr.enseirb.pirtoupire.centralized.AbstractMessage.ProtocolException;
import fr.enseirb.pirtoupire.centralized.Manager;
import fr.enseirb.pirtoupire.centralized.Torrent;
import fr.enseirb.pirtoupire.io.FilesystemTorrent;

public class Screen extends JFrame implements Observer {
	// private final JPanel bar;
	private final JTable searchTable;
	private final ResultTableModel searchModel;
	private final JTable downloadTable;
	private final DownloadTableModel downloadModel;

	private final TextField searchNameInput;
	private final JSlider searchSizeInput;

	private final Action startDownload;

	public Screen() {

		this.setTitle("PirToPire");
		this.setSize(1000, 700);
		this.setLocationRelativeTo(null);
		JSplitPane main = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		this.setContentPane(main);

		// left panel (search)
		// search form
		searchNameInput = new TextField();
		searchSizeInput = new JSlider(0, 2000, 500);
		searchSizeInput.setPaintLabels(true);
		searchSizeInput.setLabelTable(searchSizeInput.createStandardLabels(500));
		// searchNameInput.setMaximumSize(new Dimension(Integer.MAX_VALUE, 25));
		JPanel searchPanel = new JPanel(new GridBagLayout());
		searchPanel.setBorder(BorderFactory.createTitledBorder("Rechercher"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(8, 8, 8, 8);
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0;

		searchPanel.add(new JLabel("Nom"), c);
		c.weightx = 1;
		c.gridx = 1;
		searchPanel.add(searchNameInput, c);
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0;
		searchPanel.add(new JLabel("Taille"), c);
		c.gridx = 1;
		c.weightx = 1;
		searchPanel.add(searchSizeInput, c);

		final JButton searchBut = new JButton("Rechercher");
		searchBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!searchNameInput.getText().equals("")) {
					searchNameInput.setEnabled(false);
					searchSizeInput.setEnabled(false);
					searchBut.setEnabled(false);
					Thread search = new Thread() {
						@Override
						public void run() {
							try {
								Set<TorrentFile> res = Manager.trackerLook("filename=\""
										+ searchNameInput.getText() + "\" filesize>\"" + searchSizeInput.getValue() + "\"");
								searchNameInput.setText("");
								((ResultTableModel) getSearchTable().getModel()).setData(res);
								new ButtonColumn(searchTable, startDownload, 3);
							} catch (ProtocolException ex) {

							} finally {
								searchNameInput.setEnabled(true);
								searchSizeInput.setEnabled(true);
								searchBut.setEnabled(true);
							}
						};

					};
					search.start();
				}
			}
		});
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 2;
		searchPanel.add(searchBut, c);

		searchModel = new ResultTableModel();
		searchTable = new JTable(searchModel);
		searchTable.getColumn("Action").setMinWidth(150);
		searchTable.getColumn("Action").setMaxWidth(150);
		searchTable.setAutoCreateRowSorter(true);
		searchTable.setRowHeight(25);
		searchTable.setBorder(BorderFactory.createLineBorder(Color.black));

		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 2;
		c.weightx = 1;
		c.weighty = 2;
		searchPanel.add(new JScrollPane(searchTable), c);

		main.add(searchPanel, 0);

		// right panel (list of downloads)
		JPanel dlPanel = new JPanel(new GridBagLayout());
		dlPanel.setBorder(BorderFactory.createTitledBorder("Téléchargements"));

		final JButton seedBut = new JButton("Seeder un fichier");
		seedBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File seedfile = chooser.getSelectedFile();
					TorrentFile tf = new TorrentFile(seedfile);
					try {
						FilesystemTorrent tfs = Manager.seedNewTorrent(tf, seedfile);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		dlPanel.add(seedBut, c);

		downloadModel = new DownloadTableModel();
		downloadTable = new JTable(downloadModel);
		// downloadTable.getColumn("Action").setMinWidth(150);
		// downloadTable.getColumn("Action").setMaxWidth(150);
		downloadTable.getColumn("Progrès").setCellRenderer(new ProgressRenderer());
		downloadTable.setAutoCreateRowSorter(true);
		downloadTable.setRowHeight(25);
		downloadTable.setBorder(BorderFactory.createLineBorder(Color.black));

		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 2;
		dlPanel.add(new JScrollPane(downloadTable), c);

		main.add(dlPanel, 1);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		startDownload = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				int row = Integer.valueOf(e.getActionCommand());
				TorrentFile tf = searchModel.getTorrent(row);
				JFileChooser chooser = new JFileChooser();
				int returnVal = chooser.showSaveDialog(rootPane);
				if (returnVal != JFileChooser.APPROVE_OPTION) {
					return;
				}
				File leeched_file = chooser.getSelectedFile();
				if (leeched_file.exists()) {
					if (JOptionPane.showConfirmDialog(rootPane, "Overwrite existing file?") == JOptionPane.NO_OPTION) {
						return;
					}
				}
				try {
					Manager.leechNewTorrent(tf, leeched_file);
					updateDownloads();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		};

		Manager.getObservable().addObserver(this);
		Manager.getObservable().updated();
		this.setVisible(true);
	}

	public JTable getSearchTable() {
		return searchTable;
	}

	public JTable getDownloadTable() {
		return downloadTable;
	}

	private void updateDownloads() {
		System.out.println("UPDATE DOWNLOADS");
		Set<Torrent> torrents = new HashSet<Torrent>(Manager.getActiveTorrents());
		for (Torrent t : torrents) {
			t.addObserver(this);
		}
		downloadModel.setData(torrents);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof Torrent) {
			// Torrent t = (Torrent) o;
			downloadModel.dataUpdated();
		} else if (o instanceof Manager.ActiveTorrentObservable) {
			updateDownloads();
		}
	}

	private class ProgressRenderer extends DefaultTableCellRenderer implements ActionListener {

		private final JProgressBar bar = new JProgressBar(0, 100);

		public ProgressRenderer() {
			super();
			setOpaque(true);
			bar.setStringPainted(true);
			bar.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
		}

		@Override
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			Float f = (Float) value * 100f;
			bar.setValue(Math.round(f));
			String t;
			if (f == 100.0) {
				t = "Terminé";
			} else {
				t = String.format("%.2f%%", f);
			}
			bar.setString(t);
			return bar;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
		}

	}
}
