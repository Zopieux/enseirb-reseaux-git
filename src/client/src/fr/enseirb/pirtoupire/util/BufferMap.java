package fr.enseirb.pirtoupire.util;

import java.util.BitSet;

/**
 * A small wrapper over BitSet to implement a fixed-size BitSet with fixed-size byte representations.
 */
public class BufferMap extends BitSet {
	private static final long serialVersionUID = 3333717811673321294L;
	private final int length;

	public BufferMap(int length) {
		super(length);
		this.length = length;
	}

	public int capacity() {
		return length;
	}

	@Override
	public BufferMap get(int i, int j) {
		if (i >= capacity() || j > capacity()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		BufferMap bm = new BufferMap(j - i);
		bm.or(super.get(i, j));
		return bm;
	}

	@Override
	public boolean get(int i) {
		if (i >= capacity()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return super.get(i);
	}

	@Override
	public void set(int i) {
		if (i >= capacity()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		super.set(i);
	}

	@Override
	public void set(int i, int j) {
		if (i >= capacity() || j > capacity()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		super.set(i, j);
	}

	@Override
	public void clear(int i, int j) {
		if (i >= capacity() || j > capacity()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		super.clear(i, j);
	}

	@Override
	public void clear(int i) {
		if (i >= capacity()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		super.clear(i);
	}

	@Override
	public void flip(int i) {
		if (i >= capacity()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		super.flip(i);
	}

	@Override
	public void flip(int i, int j) {
		if (i >= capacity() || j > capacity()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		super.flip(i, j);
	}

	public void fill(boolean value) {
		set(0, capacity(), value);
	}

	@Override
	public byte[] toByteArray() {
		byte[] bytes = new byte[(capacity() + 7) / 8];
		for (int i = 0; i < capacity(); i++) {
			if (get(i))
			{
				bytes[i / 8] |= 1 << (i % 8);
			}
		}
		return bytes;
	}

	public static BufferMap valueOf(byte[] bytes, int length) {
		BufferMap bm = new BufferMap(length);
		assert bytes.length == (length + 7) / 8;
		for (int i = 0; i < length; i++) {
			if ((bytes[i / 8] & (1 << (i % 8))) > 0) {
				bm.set(i);
			}
		}
		return bm;
	}

	@Override
	public String toString() {
		return "BM { size: " + capacity() + ", set true: " + cardinality() + " }";
	}

	@Override
	public BufferMap clone() {
		BufferMap bm = new BufferMap(capacity());
		bm.or(this);
		return bm;
	}

	public static int byteLengthOf(int l) {
		return (l + 7) / 8;
	}
}
