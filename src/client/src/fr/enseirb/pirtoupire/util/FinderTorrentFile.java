package fr.enseirb.pirtoupire.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import fr.enseirb.pirtoupire.Configuration;
import fr.enseirb.pirtoupire.centralized.Manager;
import fr.enseirb.pirtoupire.io.TorrentMetadata;

public class FinderTorrentFile {

	public FinderTorrentFile() {
		super();
	}

	public static List<File> findFiles(File directory) {
		List<File> torrentfiles = new ArrayList<File>();
		if (!directory.exists()) {
			System.out.println("Le repertoire '" + directory + "' n'existe pas");
		} else if (!directory.isDirectory()) {
			System.out.println("Le chemin '" + directory + "' correspond à un fichier pas à un répertoire");
		} else {
			File[] files = directory.listFiles();

			for (int i = 0; i < files.length; i++) {
				if (!files[i].isDirectory() && Pattern.matches("[0-9A-F]+\\.[0-9A-F]+\\.meta", files[i].getName())) {
					torrentfiles.add(files[i]);
				}
			}
		}
		return torrentfiles;
	}

	public static void loadTorrentFile() {
		List<File> torrentfiles = FinderTorrentFile.findFiles(Configuration.SESSION_DIRECTORY);
		for (File file : torrentfiles) {
			System.out.println(file.getName());
			TorrentMetadata metafile = null;
			try {
				metafile = new TorrentMetadata(file);

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				Manager.newTorrentFromMetadata(metafile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
