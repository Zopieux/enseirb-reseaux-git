package fr.enseirb.pirtoupire.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {
	final private static String HASH_METHOD = "SHA-1";
	final public static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

	/**
	 * http://stackoverflow.com/a/9855338/180709
	 */
	public static String byteArrayToHexString(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = bytes.length - 1; j >= 0; j--) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static String computeStringHash(String string) {
		try {
			MessageDigest digest = MessageDigest.getInstance(HASH_METHOD);
			digest.update(string.getBytes("UTF-8"));
			return byteArrayToHexString(digest.digest());
		} catch (NoSuchAlgorithmException e) {
			throw new InternalError(e.toString());
		} catch (UnsupportedEncodingException e) {
			throw new InternalError(e.toString());
		}
	}

	public static String computeFileHash(File file) {
		try {
			InputStream stream = new FileInputStream(file);
			MessageDigest digest = MessageDigest.getInstance(HASH_METHOD);
			int n = 0;
			byte[] buff = new byte[1 << 12];
			while (n != -1) {
				n = stream.read(buff);
				if (n > 0) {
					digest.update(buff, 0, n);
				}
			}
			stream.close();
			return byteArrayToHexString(digest.digest());
		} catch (NoSuchAlgorithmException e) {
			throw new InternalError(e.toString());
		} catch (IOException e) {
			throw new InternalError(e.toString());
		}
	}

	public static String implode(String separator, String... data) {
		if (data.length == 0) {
			return "";
		}
		if (data.length == 1) {
			return data[0];
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < data.length - 1; i++) {
			sb.append(data[i]);
			sb.append(separator);
		}
		sb.append(data[data.length - 1]);
		return sb.toString();
	}
}
