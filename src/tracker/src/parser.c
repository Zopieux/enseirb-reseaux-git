#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include "parser.h"
#include "tracker.h"


typedef char* (*action_c)(struct peer *, char*);
action_c action[NB_COMMAND] = {do_announce,
        do_look,
        do_getfile,
        do_update,
};


char* next_word_sep(char **string, int sep){
  char *tmp, *pch;
  if (pch = strchr(*string, sep)) {
    tmp = strndup(*string, pch - *string);
    *string = pch + 1;
  }
  else {
    tmp = strdup(*string);
    *string = NULL;
    }
  return tmp;
}

char* next_word (char **string){
  return next_word_sep(string, 32);
}

char **extract(char *regex, char *source, int argc) {

  size_t maxGroups = argc + 1;
  char **res = (char **) malloc(argc*sizeof(char *));
  char buf[1000];

  regex_t regexCompiled;
  regmatch_t groupArray[maxGroups];

  if (regcomp(&regexCompiled, regex, REG_EXTENDED)) {
      printf("Could not compile regular expression.\n");
      exit(1);
  }

  char sourceCopy[strlen(source) + 1];
  strcpy(sourceCopy, source);
  if (!regexec(&regexCompiled, source, maxGroups, groupArray, 0)) {
      unsigned int g = 0;
      for (g = 1; g < maxGroups; g++) {
          if (groupArray[g].rm_so == (size_t)-1)
            break;
          sourceCopy[groupArray[g].rm_eo] = 0;
          sprintf(buf, "%s", sourceCopy + groupArray[g].rm_so);
          res[g-1] = strdup(buf);
        }
  }
  else
    return NULL;

  regfree(&regexCompiled);
  return res;
}

char* do_look(struct peer *p, char *msg) {
  char **res, *regex, *criterious, *criterious_name, *test, *pch, *name, *number;
  char bigbuf[4*BUFFER_SIZE]="\0";
  char buffer[BUFFER_SIZE]="\0";
  char* criteriousTab[NB_CRITERIOUS_MAX];
  struct peer *peer;
  struct file *file;
  GSList *list_files = files;
  int criterious_nb = 0, cur_crit = 0, accepted = 0, n = 0;
  if(msg == NULL)
    return NULL;

  regex = "\\[(.*)\\]";
  if (!(res = extract(regex, msg, 3)))
    return NULL;

  criterious = res[0];
  sprintf(bigbuf,"list [");

  while(criterious)
    {
      test = next_word(&criterious);
      if(strlen(test))
        criteriousTab[criterious_nb++] = test;
    }

  char deli1;
  char* cur_crit_ptr;
  const char deli2 = '"';
  int sep =0; 
  int sep2=34; //ascii : "
  if(criterious_nb!=0){
    while(list_files!=NULL)
      {
  file = list_files->data;
  accepted=1;
  while(cur_crit< criterious_nb)
    {
      if(strchr(criteriousTab[cur_crit],60)!=NULL)
        sep=60;//deli1 = '<';
      else if(strchr(criteriousTab[cur_crit],62)!=NULL)
        sep=62;//deli1 = '>';
      else if(strchr(criteriousTab[cur_crit],61)!=NULL)
        sep=61;//deli1 = '=';
      else 
        return NULL;
      cur_crit_ptr = criteriousTab[cur_crit];
      criterious_name = next_word_sep(&cur_crit_ptr,sep);  
      /* ### name test part ### */
      if(!strcmp(criterious_name,"filename")){
        name = next_word_sep(&cur_crit_ptr,sep2);
        name = next_word_sep(&cur_crit_ptr,sep2);
        if(strcmp(name, file->name)){
    accepted=0;
        }
      }
      
      /* ### size test part ### */
      else if(!strcmp(criterious_name,"filesize")){ 
        number = next_word_sep(&cur_crit_ptr,sep2);
        number = next_word_sep(&cur_crit_ptr,sep2);
        switch(sep)
    {
    case 60://<
      if(!(file->length<atoi(number)))
        accepted=0;
      break;
    case 61://=
      if(!(file->length==atoi(number)))
        accepted=0;
      break;
    case 62://>
      if(!(file->length>atoi(number)))
        accepted=0;
      break;
    default:
      accepted=0;
      break;
    }
      }
      cur_crit++;
    }
  if(accepted){
    sprintf(buffer,"%s %d %d %s "
      , file->name
      , file->length
      , file->pieceSize
      , file->key);
    strcat(bigbuf, buffer);
    n = 1;
  }
  cur_crit=0;
  list_files = list_files->next;
      }
  }
  strcpy(bigbuf + strlen(bigbuf) - n, "]\n");
  free(res[0]);
  free(res);
  return strdup(bigbuf);
}

char* do_announce(struct peer *p, char *msg) {
  char **res, *seed, *leech, *buffer, *regex, *name, *tmp, *key, *str;
  int length, pieceSize, i;
  
  regex = "listen ([0-9]+) seed \\[([^\]]*)\\] leech \\[([^\]]*)\\]";
  if (!(res = extract(regex, msg, 3)))
    return NULL;

  p->portnb = atoi(res[0]);
  printf("new client : %s %d (%p)\n", p->ip, p->portnb, p);
  seed = res[1];
  leech = res[2];
  
  if (strcmp(seed, "")) {
    str = seed;
    while (str) {
      name = next_word(&str);
      tmp = next_word(&str);
      length = atoi(tmp);
      free(tmp);
      tmp = next_word(&str);
      pieceSize = atoi(tmp);
      free(tmp);
      key = next_word(&str);
      add_file(p, _SEED_, name, length, pieceSize, key);
    }
  }
  /* ### parsing of the leech part ###*/
  if(strcmp(leech,"")) {
      str = leech;
      while(str) {
        key =  next_word(&str);
        add_file(p, _LEECH_, NULL, 0, 0, key);
      }
  }
  for (i = 0; i < 3; i++)
    free(res[i]);
  free(res);
  buffer = "ok\n";
  return strdup(buffer);
}

char* do_getfile(struct peer *p, char *msg) {
  char **res, *key, *response, *regex;
  struct file *file;
  struct peer *peer;
  char buffer[BUFFER_SIZE];
  char buff_conc[3*BUFFER_SIZE];
  GSList *peers;
  int n = 0;

  regex = "(.*)";
  if (!(res = extract(regex, msg, 1)))
    return NULL;

  key = res[0];
  if (!(file = g_hash_table_lookup(hash_table, key))){
    free(res[0]);
    free(res);
    return strdup("peers []\n");
  }

  peers = file->seeds;
  sprintf(buff_conc, "peers %s [", key);
  while (peers) {
    peer = peers->data;
    sprintf(buffer, "%s:%hd ", peer->ip, peer->portnb);
    strcat(buff_conc, buffer);
    peers = peers->next;
    n = 1;
  } 
  strcpy(buff_conc + strlen(buff_conc) - n, "]\n");
  response = strdup(buff_conc);
  free(res[0]);
  free(res);
  return response;
}

void free_key_list (gpointer data) {
  free(data);
}

char* do_update (struct peer *p, char *msg) {
  char **res, *seed, *leech, *regex, *key, *buffer;
  GSList *key_list_leech = NULL, *key_list_seed = NULL;

  regex = "seed \\[([^\]]*)\\] leech \\[([^\]]*)\\]";
  if (!(res = extract(regex, msg, 2)))
    return NULL;

  seed = res[0];
  leech = res[1];

  /* ### parsing of the seed part  ###*/
  if(strcmp(seed,""))
    while(seed) {
      key = next_word(&seed);
      key_list_seed = g_slist_prepend(key_list_seed, key);
    }
  /* ### parsing of the leech part  ###*/
  if(strcmp(leech, ""))
    while(leech) {
      key = next_word(&leech);
      key_list_leech = g_slist_prepend(key_list_leech, key);
    }

  update_file(p, key_list_seed, key_list_leech);
  g_slist_free_full(key_list_seed, free_key_list);
  free(res[0]);
  free(res[1]);
  free(res);
  buffer = "ok\n";
  return strdup(buffer);
}

char* parser(struct peer *p, char* myString)
{
  int i;
  char *ret;
  char *firstWord = next_word(&myString);
  for(i = 0; i < NB_COMMAND; i++)
  {
    if(myString && !(strcmp(commands[i], firstWord))){
      ret = action[i](p, myString);
      free(firstWord);
      return ret;
    }
  }
  free(firstWord);
  free(myString);
  return NULL;
}
