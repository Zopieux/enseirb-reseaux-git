#ifndef _PARSER_H_
#define _PARSER_H_

#include "tracker.h"

#define str(x) # x
#define xstr(x) str(x)

#define LENGTH 512
#define NB_CRITERIOUS_MAX 10
#define NB_COMMAND 4

static char* commands[NB_COMMAND]={"announce",
				   "look",
				   "getfile",
				   "update",
};

char* do_announce(struct peer *, char*); //ok
char* do_look(struct peer *, char*); //mal fait,et prend en compte que les nom
char* do_getfile(struct peer *, char*); //ok
char* do_update(struct peer *, char*); //ok

char* next_word (char **);
char* next_word_sep(char **, int);
char* parser(struct peer *, char*);

#endif
