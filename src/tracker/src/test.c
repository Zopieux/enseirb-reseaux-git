#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tracker.h"
#include "parser.h"

void print_network_database();
void debug();
void this_is_a_test_name ();
struct peer *p;

void proccess_(char *msg){
    char *response;
    char *msg_ = strdup(msg);
    printf("<< %s\n",msg);
    if ((response = parser(p, msg_))){
        printf(">> %s", response);
        free(response);
    }
    else
        printf("====== parser returned NULL ======\n");
    free(msg_);
}


int main () {
    hash_table = g_hash_table_new (g_str_hash, g_str_equal);
    p = new_peer();

    this_is_a_test_name();

    remove_peer(p);
    g_hash_table_destroy(hash_table);
    return 0;
}

