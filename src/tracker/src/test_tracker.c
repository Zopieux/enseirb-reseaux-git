#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tracker.h"
#include "parser.h"

void print_network_database();
void debug();
void this_is_a_test_name ();

void proccess_(struct peer *p, char *msg){
    char *response;
    char *msg_ = strdup(msg);
    printf("<< %s\n",msg);
    if ((response = parser(p, msg_))){
        printf(">> %s", response);
        free(response);
    }
    else
        printf("====== parser returned NULL ======\n");
    free(msg_);
}


int main () {
    hash_table = g_hash_table_new (g_str_hash, g_str_equal);
    
    struct peer *p1 = new_peer();
    struct peer *p2 = new_peer();

    proccess_(p1, "announce listen 2222 seed [file_a.dat 2097152 1024 8905e92afeb80fc7722ec89eb0bf0966] leech []");
    proccess_(p2, "announce listen 2223 seed [] leech []");
    proccess_(p2, "update seed [8905e92afeb80fc7722ec89eb0bf0966] leech []");
    proccess_(p2, "update seed [] leech []");
    proccess_(p2, "look [filename=\"file_a.dat\"]");
    proccess_(p2, "getfile 8905e92afeb80fc7722ec89eb0bf0966");

    remove_peer(p1);
    remove_peer(p2);
    
    g_hash_table_destroy(hash_table);
    return 0;
}

