#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include "tracker.h"


char* parser(struct peer *, char*);


void error(char *msg) {
    perror(msg);
    exit(1);
}


GSList *files, *peers;
GHashTable *hash_table;
unsigned clients_nb;


struct peer* new_peer(){
    struct peer *p = (struct peer*) calloc(1,sizeof(struct peer));
    peers = g_slist_prepend(peers, p);
    return p;
}

void free_file (struct file *f){
    g_hash_table_remove(hash_table, f);
    free(f->name);
    free(f->key);
    free(f);
}

void remove_peer (struct peer *p){
    GSList *file = p->files;
    struct file *f;
    while (file){
        f = file->data;
        f->leeches = g_slist_remove(f->leeches, p);
        f->seeds = g_slist_remove(f->seeds, p);
        // si plus personne ne seed le fichier, on le retire du réseau
        if (!f->seeds){
            files = g_slist_remove(files, f);
            free_file(f);
        }
        file = file->next;
    }
    peers = g_slist_remove(peers, p);
    free(p->ip);
    free(p);
}

void clear_database () {
    GSList *p = peers;
    while (p){
        remove_peer(p->data);
        p = p->next;
    }
}

void fill_file(struct file *f, struct peer *p, char *name, unsigned int length, unsigned int pieceSize, char *key){
    f->name = name;
    f->length = length;
    f->pieceSize = pieceSize;
    f->key = key;
}

void add_file (struct peer *p, unsigned int type, char *name, unsigned int length, unsigned int pieceSize, char *key) {
    struct file *file = g_hash_table_lookup (hash_table, key);
    switch (type) {
        case _LEECH_:
        free(key);
        if (file && !g_slist_find(file->leeches, p))
            file->leeches = g_slist_prepend(file->leeches, p);
        break;
        case _SEED_:
        if (!file){
            file = (struct file *) calloc(1, sizeof(struct file));
            fill_file(file, p, name, length, pieceSize, key);
            files = g_slist_prepend (files, file);
        }
        else {
            free(key);
            free(name);
        }
        if (!g_slist_find(file->seeds, p))
            file->seeds = g_slist_prepend(file->seeds, p);
        else
            printf("!!! peer was allready in file->seeds !!!\n");
        break;
    }
    key = file->key;
    g_hash_table_insert (hash_table, key, file);

    if (!g_slist_find(p->files, file))
        p->files = g_slist_prepend(p->files, file);
    else
        printf("!!! file was allready in peer->files !!!\n");
}

gint find_key (gconstpointer a, gconstpointer b) {
    char *ka = (char *) a, *kb = (char *) b;
    return strcmp(ka, kb);
}

void update_file (struct peer *p, GSList *key_list_seed, GSList *key_list_leech) {
    GSList *keys, *k;
    struct file *f;
    GSList *file;
    for (keys = key_list_seed; keys; keys = keys->next) {
        if (!(f = g_hash_table_lookup(hash_table, keys->data)))
            continue;
        if (!g_slist_find(f->seeds, p))
            f->seeds = g_slist_prepend(f->seeds, p);
    }
    for (keys = key_list_leech; keys; keys = keys->next) {
        if (!(f = g_hash_table_lookup(hash_table, keys->data)))
            continue;
        if (!g_slist_find(f->leeches, p))
            f->leeches = g_slist_prepend(f->leeches, p);
    }
    for (file = p->files; file; file = file->next) {
        f = file->data;
        if (!g_slist_find_custom(key_list_seed, f->key, find_key))
            f->seeds = g_slist_remove(f->seeds, p);
        if (!g_slist_find_custom(key_list_leech, f->key, find_key))
            f->leeches = g_slist_remove(f->leeches, p);
    }
    keys = g_slist_concat(key_list_seed, key_list_leech);
    for (k = keys; k; k = k->next) {
        if (!(f = g_hash_table_lookup(hash_table, k->data)))
            continue;
        if (!g_slist_find(p->files, f))
            p->files = g_slist_prepend(p->files, f);
    }
    for (file = p->files; file; file = file->next) {
        f = file->data;
        if (!g_slist_find_custom(keys, f->key, find_key))
            p->files = g_slist_remove(p->files, f);
    }
}

void print_network_database () {
    printf("________________PRINT__DATABASE________________\n");
    GSList *peer;
    struct peer *p;
    struct file *f;
    GSList *files;
    for (peer = peers; peer; peer = peer->next) {
        p = peer->data;
        printf("peer coordinates : [ip = %s, port number = %hd] (%p)\n", p->ip, p->portnb, p);
        printf("seeding :\n");
        for (files = p->files; files; files = files->next) {
            f = files->data;
            if (g_slist_find(f->seeds, p))
                printf("\t%s %d %d %s %p\n", f->name, f->length,f->pieceSize, f->key, f);
        }
        printf("leeching :\n");
        for (files = p->files; files; files = files->next) {
            f = files->data;
            if (g_slist_find(f->leeches, p))
                printf("\t%s %d %d %s %p\n", f->name, f->length,f->pieceSize, f->key, f);
        }
    }
    printf("_______________________________________________\n");
}

void print_pointer (gpointer data, gpointer user_data) {
    printf("%p ", data);
}

void print_key (gpointer data, gpointer user_data) {
    char *key = (char *) data;
    printf("%s", key);
}

void print_file (gpointer data, gpointer user_data) {
    struct file *f = (struct file*) data;
    GSList *seeds = f->seeds, *leeches = f->leeches;
    printf("file %s\n", f->name);
    printf("\tseeds : ");
    g_slist_foreach(seeds, print_pointer, NULL);
    printf("\n\tleeches : ");
    g_slist_foreach(leeches, print_pointer, NULL);
    printf("\n");
}

void debug () {
    printf("_____________________DEBUG_____________________\n");
    g_slist_foreach(files, print_file, NULL);
    printf("_______________________________________________\n");
}


void proccess (char * buffer, struct peer *p) {
    if (!strcmp(buffer, "print")){
        print_network_database();
        return;
    }
    char *msg = strdup(buffer);
    char *response = parser(p, msg);
    if (!response)
        return;
    if (send(p->sockfd, response, strlen(response), 0) < 0)
        error("send");
    free(response);
}

void exit_listen_socket (struct peer *p, int i){
    clients_nb--;
    remove_peer(p);
    pthread_exit((void*) i);
}

void* listen_socket(void* arg){

    int received;
    unsigned int msg_complete, n;
    char buffer[BUFFER_SIZE];
    char *buffer_tmp, *pch, *pch2;
    struct peer *p = (struct peer*) arg;

    n = 0;
    while (1) {
        msg_complete = 0;
        buffer_tmp = buffer + n;
        while (!msg_complete) {
            if ((received = recv(p->sockfd, buffer_tmp, PACKET_SIZE, 0)) < 0) {
                perror("recv()");
                exit_listen_socket(p, errno);
            }
            if (!received){
                printf("client déconnécté\n");
                exit_listen_socket(p, EXIT_SUCCESS);
            }
            buffer_tmp += received * sizeof(char);
            if ((buffer_tmp - buffer) > BUFFER_SIZE) {
                fprintf(stderr, "buffer overflow\n");
                exit_listen_socket(p, EXIT_FAILURE);
            }
            if ((pch = strstr(buffer,"\n"))){
                strcpy(pch, "\0");
                msg_complete = 1;
                pch++;
            }
        }
        proccess(buffer, p);
        if ((n = buffer_tmp - pch) > 0){
            while ((pch2 = strstr(pch,"\n"))){
                strcpy(pch2, "\0");
                proccess(pch, p);
                pch = pch2 + 1;
                n = buffer_tmp - pch;
            }
            strncpy(buffer, pch, n);
        }
    }
}


