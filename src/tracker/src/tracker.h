#ifndef _TRACKER_H_
#define _TRACKER_H_

#include <glib.h>

#define PTHREAD_CREATE(thread, func, arg) if(pthread_create(thread, NULL, func, arg)){perror("pthread_create"); return EXIT_FAILURE;}


#define BUFFER_SIZE 1000
#define PACKET_SIZE 100
#define MAX_CLIENTS_NB 20

#define _SEED_ 1
#define _LEECH_ 2

struct file {
  char *name;
  unsigned int length;
  unsigned int pieceSize;
  char *key;
  GSList *seeds;
  GSList *leeches;
};

struct peer {
  unsigned short portnb;
  char *ip;
  int sockfd;
  GSList *files;
};

void proccess(char*, struct peer*);
void* listen_socket(void*);
void error(char*);
void add_portnb(struct peer*, unsigned short);
struct peer* new_peer();
void remove_peer(struct peer*);
struct peer* last_peer();

void add_file(struct peer *, unsigned int, char *, unsigned int, unsigned int, char *);
void update_file(struct peer*, GSList*, GSList*);
struct file* last_file(struct file*);
void fill_file(struct file *, struct peer*, char *, unsigned int, unsigned int,char *);

void print_network_database();
void print_pointer (gpointer, gpointer);
void print_key (gpointer, gpointer);
void print_file (gpointer data, gpointer user_data);
void debug();


extern GSList *files, *peers;
extern GHashTable *hash_table;
extern unsigned clients_nb;


#endif
