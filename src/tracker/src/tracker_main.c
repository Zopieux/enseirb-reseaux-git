#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "tracker.h"


int main(int argc, char** argv) {
    if(argc < 2) {
        printf("Usage: %s listen_port\n", argv[0]);
        exit(1);
    }

    int sockfd, newsockfd, clilen;
    unsigned short portnb;
    struct sockaddr_in serv_addr, cli_addr;
    pthread_t pthread[MAX_CLIENTS_NB];
    int i = 0;
    struct peer *p;
    char buffer[BUFFER_SIZE];
    portnb = atoi(argv[1]);
    clients_nb = 0;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portnb);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR on binding");

    listen(sockfd,5);
    clilen = sizeof(cli_addr);

    hash_table = g_hash_table_new (g_str_hash, g_str_equal);

    printf("En attente de connexion sur 0.0.0.0:%d...\n", portnb);
    while ((newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen))) {
        if (newsockfd < 0)
            error("ERROR on accept");

        if (clients_nb < MAX_CLIENTS_NB){
            p = new_peer();
            inet_ntop(AF_INET, (void*) &cli_addr.sin_addr, buffer, BUFFER_SIZE);
            p->ip = strdup(buffer);
            p->sockfd = newsockfd;
            PTHREAD_CREATE(&pthread[i], listen_socket, (void*) p);
            // printf("nouveau client connecté (%s)\n", p->ip);
            i = (i + 1) % MAX_CLIENTS_NB;
            clients_nb++;
        }
        else
            printf("client refusé : nombre maximum de clients atteint\n");
    }

    close(sockfd);
    return 0;
}
